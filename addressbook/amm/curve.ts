export const CURVE_CONTRACTS = {
  mainnetETH: {
    _3pool: {
      pool: '0xbEbc44782C7dB0a1A60Cb6fe97d0b483032FF1C7',
      lpToken: '0x6c3F90f043a72FA612cbac8115EE7e52BDe6E490',
      liquidityGauge: '0xbFcF63294aD7105dEa65aA58F8AE5BE2D9d0952A',
    }
  }
}