import { pangolinfujiVaults } from './pangolinfujiVaults';
import type { IVaultInfoTasks } from '../../typescript/vaults';

export const avaxfujiVaults: IVaultInfoTasks[] = [
  ...pangolinfujiVaults
];