import { IVaultInfoTasks } from '../../typescript/vaults';
export const pangolinfujiVaults: IVaultInfoTasks[] = [
  {
    vault: '0x77F6d9E8d54BEC6076ac8cCDCEb451bd785aF538',
    oneClick: '0x2c716BAA2A28751591D4aaEbbD6a2315Ed84DdaA',
  },
];