import { TOKENS } from '../../../../../addressbook/tokens';
import { CURVE_CONTRACTS } from '../../../../../addressbook/amm/curve';
import { UNISWAP_CONTRACTS } from '../../../../../addressbook/amm/uniswap';

/* types */
import type { DeployFunction } from 'hardhat-deploy/types';
import type { HardhatRuntimeEnvironment } from 'hardhat/types';
import type { Accounts } from '../../../../../typescript/hardhat';

const func: DeployFunction = async function(hre: HardhatRuntimeEnvironment) {
  const { deployments, ethers, getNamedAccounts, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts() as Accounts;

  // The following script can only be run on networks with the tags 'local' and 'staging'
  if(network.tags.local || network.tags.staging) {
    const {
      _3pool: {
        pool,
        lpToken,
        liquidityGauge,
      }
    } = CURVE_CONTRACTS.mainnetETH;

    const { CRV, WETH, USDT } = TOKENS.mainnetETH;
    const { Router02 } = UNISWAP_CONTRACTS.mainnetETH.V2;
    
    // this contract is upgradeable through uups (EIP-1822)
    await deploy('StrategyCurve3Pool', {
      log: true,
      from: deployer,
      proxy: {
        proxyContract: 'UUPSProxy',
        execute: {
          init: {
            methodName: 'initialize', // method to be execute when the proxy is deployed
            args: [
              lpToken, // address _want
              ethers.constants.AddressZero, // address _vault
              pool, // address _pool
              liquidityGauge, // address _liquidityGauge
              deployer, // address _keeper
              Router02, // address _unirouter
              deployer, // address _strategist
              deployer, // address _treasuryFeeRecipient
              [ CRV, WETH ], // address[] memory _outputToWrappedRoute
              [ WETH, USDT ], // address[] memory _wrappedToDepositRoute
            ]
          }
        }
      }
    })
  }
}

func.tags = ['StrategyCurve3Pool'];

export default func;