import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';

const func: DeployFunction = async function ( hre: HardhatRuntimeEnvironment ) {
  const { deployments, getNamedAccounts, ethers, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // The following script can only be run on networks with the tags 'local' or 'staging'
  if(network.tags.local || network.tags.staging) {
    const [ WAVAX, JoeRouter02 ] = await Promise.all([
      ethers.getContract('WAVAX'),
      ethers.getContract('JoeRouter02Mock'),
    ])

    // this contract is upgradeable through uups (EIP-1822)
    await deploy('OneClickTraderJoeV1', {
      log: true,
      from: deployer,
      proxy: {
        proxyContract: 'UUPSProxy',
        execute: {
          init: {
            methodName: 'initialize', // method to be execute when the proxy is deployed,
            args: [
              JoeRouter02.address, // address _router
              WAVAX.address, // address _wrapped
            ]
          }
        }
      }
    });
  }
};

func.tags = ['OneClickTraderJoeV1'];
func.dependencies = ['WAVAX', 'JoeRouter02Mock'];

export default func;