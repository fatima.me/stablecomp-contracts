import { TOKENS } from '../../../../addressbook/tokens';

/* types */
import type { DeployFunction } from 'hardhat-deploy/types';
import type { HardhatRuntimeEnvironment } from 'hardhat/types';

const func: DeployFunction = async function ( hre: HardhatRuntimeEnvironment ) {
  const { deployments, getNamedAccounts, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // The following script can only be run on networks with the tags 'local' or 'staging'
  if(network.tags.local || network.tags.staging) {
    // this contract is upgradeable through uups (EIP-1822)
    await deploy('OneClickCurveLPV1', {
      log: true,
      from: deployer,
      proxy: {
        proxyContract: 'UUPSProxy',
        execute: {
          init: {
            methodName: 'initialize', // method to be execute when the proxy is deployed,
            args: [
              TOKENS.mainnetETH.WETH, // address _wrapped
            ]
          }
        }
      }
    });
  }
};

func.tags = ['OneClickCurveLPV1'];

export default func;