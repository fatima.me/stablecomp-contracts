import { DeployFunction } from 'hardhat-deploy/types';
import { Accounts } from '../../../../typescript/hardhat';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import {
  WAVAX as IWAVAX,
  JoePair as IJoePair,
  ERC20Mock as IERC20Mock,
  JoeFactory as IJoeFactory,
  JoeRouter02Mock as IJoeRouter02Mock,
  MasterChefJoeV3 as IMasterChefJoeV3,
} from '../../../../build/types';

const func: DeployFunction = async function(hre: HardhatRuntimeEnvironment) {
  const { deployments, ethers, getNamedAccounts, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts() as Accounts;

  // The following script can only be run on networks with the tags 'local' and 'staging'
  if(network.tags.local || network.tags.staging) {
    const [
      USDC,
      USDCe,
      WAVAX,
      JoeFactory,
      JoeRouter02,
      MasterChefJoeV3,
    ] = await Promise.all([
      ethers.getContract<IERC20Mock>('USDC'),
      ethers.getContract<IERC20Mock>('USDC.e'),
      ethers.getContract<IWAVAX>('WAVAX'),
      ethers.getContract<IJoeFactory>('JoeFactory'),
      ethers.getContract<IJoeRouter02Mock>('JoeRouter02Mock'),
      ethers.getContract<IMasterChefJoeV3>('MasterChefJoeV3'),
    ]);
  
    const [ JoeToken, pair ] = await Promise.all([
      MasterChefJoeV3.JOE(),
      JoeFactory.getPair(USDC.address, USDCe.address)
    ]);

    const pairContract = await ethers.getContractAt<IJoePair>('JoePair', pair);

    const [ token0, token1 ] = await Promise.all([
      pairContract.token0(),
      pairContract.token1(),
    ]);
    
    const poolId = 0; // PAIR_USDC_USDCe see the MasterChefJoeV3 deployment file

    // this contract is upgradeable through uups (EIP-1822)
    await deploy('StrategyDualLPTraderJoe_USDC_USDC.e', {
      log: true,
      from: deployer,
      contract: 'StrategyDualLP',
      proxy: {
        proxyContract: 'UUPSProxy',
        execute: {
          init: {
            methodName: 'initialize', // method to be execute when the proxy is deployed
            args: [
              pair, // address _want
              ethers.constants.AddressZero, // address _vault
              poolId, // uint256 _poolId
              deployer, // address _keeper
              JoeRouter02.address, // address _unirouter
              deployer, // address _strategist
              MasterChefJoeV3.address, // address _masterchef
              deployer, // address _treasuryFeeRecipient
              [ JoeToken, token0 ], // address[] memory _outputToLp0Route
              [ JoeToken, token1 ], // address[] memory _outputToLp1Route
              [ JoeToken, WAVAX.address ], // address[] memory _outputToWrappedRoute
            ]
          }
        }
      }
    })
  }
}

func.tags = ['StrategyDualLPTraderJoe_USDC_USDC.e'];
func.dependencies = [
  'USDC',
  'WAVAX',
  'USDC.e',
  'JoeFactory',
  'JoeRouter02Mock',
  'MasterChefJoeV3',
  'UniswapV2Router02',
];

export default func;