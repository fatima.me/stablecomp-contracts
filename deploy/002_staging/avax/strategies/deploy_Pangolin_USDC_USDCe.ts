import { DeployFunction } from 'hardhat-deploy/types';
import { Accounts } from '../../../../typescript/hardhat';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import {
  WAVAX as IWAVAX,
  ERC20Mock as IERC20Mock,
  MiniChefV2 as IMiniChefV2,
  PangolinPair as IPangolinPair,
  PangolinRouter as IPangolinRouter,
  PangolinFactory as IPangolinFactory,
} from '../../../../build/types';

const func: DeployFunction = async function(hre: HardhatRuntimeEnvironment) {
  const { deployments, ethers, getNamedAccounts, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts() as Accounts;

  // The following script can only be run on networks with the tags 'local' and 'staging'
  if(network.tags.local || network.tags.staging) {
    const [
      USDC,
      USDCe,
      WAVAX,
      PangolinFactory,
      PangolinRouter,
      MiniChefV2,
    ] = await Promise.all([
      ethers.getContract<IERC20Mock>('USDC'),
      ethers.getContract<IERC20Mock>('USDC.e'),
      ethers.getContract<IWAVAX>('WAVAX'),
      ethers.getContract<IPangolinFactory>('PangolinFactory'),
      ethers.getContract<IPangolinRouter>('PangolinRouter'),
      ethers.getContract<IMiniChefV2>('MiniChefV2'),
    ]);
  
    const [ MiniChefV2REWARD, pair ] = await Promise.all([
      MiniChefV2.REWARD(),
      PangolinFactory.getPair(USDC.address, USDCe.address)
    ]);

    const pairContract = await ethers.getContractAt<IPangolinPair>('PangolinPair', pair);

    const [ token0, token1 ] = await Promise.all([
      pairContract.token0(),
      pairContract.token1(),
    ]);
    
    const poolId = 0; // PAIR_USDC_USDCe see the MiniChefV2 deployment file

    // this contract is upgradeable through uups (EIP-1822)
    await deploy('StrategyPangolinMiniChefLP_USDC_USDC.e', {
      log: true,
      from: deployer,
      contract: 'StrategyPangolinMiniChefLP',
      proxy: {
        proxyContract: 'UUPSProxy',
        execute: {
          init: {
            methodName: 'initialize', // method to be execute when the proxy is deployed
            args: [
              pair, // address _want
              ethers.constants.AddressZero, // address _vault
              poolId, // uint256 _poolId
              deployer, // address _keeper
              PangolinRouter.address, // address _unirouter
              deployer, // address _strategist
              MiniChefV2.address, // address _masterchef
              deployer, // address _treasuryFeeRecipient
              [ MiniChefV2REWARD, token0 ], // address[] memory _outputToLp0Route
              [ MiniChefV2REWARD, token1 ], // address[] memory _outputToLp1Route
              [ MiniChefV2REWARD, WAVAX.address ], // address[] memory _outputToWrappedRoute
            ]
          }
        }
      }
    })
  }
}

func.tags = ['StrategyPangolinMiniChefLP_USDC_USDC.e'];
func.dependencies = [
  'USDC',
  'WAVAX',
  'USDC.e',
  'MiniChefV2',
  'PangolinRouter',
  'PangolinFactory',
  'UniswapV2Router02',
];

export default func;