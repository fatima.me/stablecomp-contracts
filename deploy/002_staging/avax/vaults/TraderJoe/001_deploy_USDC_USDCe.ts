import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';

const func: DeployFunction = async function ( hre: HardhatRuntimeEnvironment ) {
  const { deployments, getNamedAccounts, ethers, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // The following script can only be run on networks with the tags 'local' and 'staging'
  if(network.tags.local || network.tags.staging) {
    const StrategyDualLP = await ethers.getContract('StrategyDualLPTraderJoe_USDC_USDC.e');
  
    // this contract is upgradeable through uups (EIP-1822)
    const deployResult = await deploy('VaultTraderJoe_USDC_USDC.e', {
      log: true,
      from: deployer,
      contract: 'StablecompVaultV1',
      proxy: {
        proxyContract: 'UUPSProxy',
        execute: {
          init: {
            methodName: 'initialize', // method to be execute when the proxy is deployed
            args: [
              StrategyDualLP.address, // IStrategy _strategy
              'Scomp Trader Joe USDC-USDC.e', // _name
              'scompTraderJoeUSDC-USDC.e', // _symbol
              21600, // uint256 _approvalDelay
            ],
          }
        }
      }
    });
  
    await StrategyDualLP.setVault(deployResult.address);
  }
};

func.tags = ['VaultTraderJoe_USDC_USDC.e'];
func.dependencies = ['StrategyDualLPTraderJoe_USDC_USDC.e'];

export default func;