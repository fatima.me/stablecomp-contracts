import { Accounts } from '../typescript/hardhat';
import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';

import {
  ERC20Mock as IERC20,
  WrappedFtm as IWrappedFtm,
  MasterChef as IMasterChef,
  UniswapV2Factory as IUniswapV2Factory,
  UniswapV2Router02 as IUniswapV2Router02,
} from '../build/types';

const func: DeployFunction = async function(hre: HardhatRuntimeEnvironment) {
  const { deployments, ethers, getNamedAccounts, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts() as Accounts;

  // The following script can only be run on networks with the tags 'local'
  if(network.tags.local) {
    const [
      USDC,
      WrappedFtm,
      MasterChef,
      UniswapV2Factory,
      UniswapV2Router02,
    ] = await Promise.all([
      ethers.getContract<IERC20>('USDC'),
      ethers.getContract<IWrappedFtm>('WrappedFtm'),
      ethers.getContract<IMasterChef>('MasterChef'),
      ethers.getContract<IUniswapV2Factory>('UniswapV2Factory'),
      ethers.getContract<IUniswapV2Router02>('UniswapV2Router02'),
    ]);
  
  
    const [ booToken, pair ] = await Promise.all([
      MasterChef.boo(),
      UniswapV2Factory.getPair(USDC.address, WrappedFtm.address)
    ])
  
    const poolId = 1; // PAIR_USDC_WFTM see the MasterChefSpookySwap deployment file
  
    // this contract is upgradeable through uups (EIP-1822)
    await deploy('StrategyMasterChefLPSpooky', {
      log: true,
      from: deployer,
      proxy: {
        proxyContract: 'UUPSProxy',
        execute: {
          init: {
            methodName: 'initialize', // method to be execute when the proxy is deployed
            args: [
              pair, // address _want
              ethers.constants.AddressZero, // address _vault
              booToken, // address _output => SpookySwap: BOO Token
              poolId, // uint256 _poolId
              deployer, // address _keeper
              WrappedFtm.address, // address _wrapped
              UniswapV2Router02.address, // address _unirouter
              deployer, // address _strategist
              MasterChef.address, // address _masterchef
              deployer, // address _treasuryFeeRecipient
            ]
          }
        }
      }
    })
  }
}

func.tags = ['StrategyMasterChefLPSpooky'];
func.dependencies = [
  'USDC',
  'WrappedFtm',
  'UniswapV2Factory',
  'UniswapV2Router02',
  'MasterChefSpookySwap',
];

export default func;