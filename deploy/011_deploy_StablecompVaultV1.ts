import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { StrategyMasterChefLPSpooky as IStrategyMasterChefLPSpooky } from '../build/types';

const func: DeployFunction = async function ( hre: HardhatRuntimeEnvironment ) {
  const { deployments, getNamedAccounts, ethers, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // The following script can only be run on networks with the tags 'local'
  if(network.tags.local) {
    const StrategyMasterChefLPSpooky = await ethers.getContract(
      'StrategyMasterChefLPSpooky'
    ) as IStrategyMasterChefLPSpooky;
  
    // this contract is upgradeable through uups (EIP-1822)
    const deployResult = await deploy('StablecompVaultV1', {
      log: true,
      from: deployer,
      proxy: {
        proxyContract: 'UUPSProxy',
        execute: {
          init: {
            methodName: 'initialize', // method to be execute when the proxy is deployed
            args: [
              StrategyMasterChefLPSpooky.address, // IStrategy _strategy
              'Scomp Spooky USDC-FTM', // _name
              'scompSpookyUSDC-FTM', // _symbol
              21600, // uint256 _approvalDelay
            ],
          }
        }
      }
    });
  
    await StrategyMasterChefLPSpooky.setVault(deployResult.address);
  }
};

func.tags = ['StablecompVaultV1'];
func.dependencies = ['StrategyMasterChefLPSpooky'];

export default func;