import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';

const func: DeployFunction = async function ( hre: HardhatRuntimeEnvironment ) {
  const { deployments, getNamedAccounts, ethers, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // The following script can only be run on networks with the tags 'local'
  if(network.tags.local) {
    const [ WETH, UniswapV2Router02 ] = await Promise.all([
      ethers.getContract('WrappedFtm'),
      ethers.getContract('UniswapV2Router02'),
    ])
  
    // this contract is upgradeable through uups (EIP-1822)
    await deploy('OneClickUniV2', {
      log: true,
      from: deployer,
      proxy: {
        proxyContract: 'UUPSProxy',
        execute: {
          init: {
            methodName: 'initialize', // method to be execute when the proxy is deployed,
            args: [
              UniswapV2Router02.address, // address _router
              WETH.address, // address _wrapped
            ]
          }
        }
      }
    });
  }
};

func.tags = ['OneClickUniV2'];
func.dependencies = ['WrappedFtm', 'UniswapV2Router02'];

export default func;