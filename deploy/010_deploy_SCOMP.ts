import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { DeployFunction } from "hardhat-deploy/dist/types";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // The following script can only be run on networks with the tags 'local' or 'staging'
  if(network.tags.local || network.tags.staging) {
    // this contract is upgradeable through uups (EIP-1822)
    await deploy('SCOMP', {
      log: true,
      from: deployer,
      proxy: {
        proxyContract: 'UUPSProxy',
        execute: {
          init: {
            methodName: 'initialize', // method to be executed when the proxy is deployed
            args: [
              "Stablecomp SCOMP", // name_
              "SCOMP" // symbol_
            ]
          }
        }
      }
    });
  }
}

func.tags = ['SCOMP'];

export default func;