import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import {
  ERC20Mock as IERC20Mock,
  PangolinFactory as IPangolinFactory,
} from '../../build/types';

const func: DeployFunction = async function ( hre: HardhatRuntimeEnvironment ) {
  const { deployments, getNamedAccounts, ethers, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // The following script can only be run on networks with the tags 'local' and 'staging'
  if(network.tags.local || network.tags.staging) {
    // Contract used for testing purposes only
    const deployResult = await deploy('PangolinFactory', {
      log: true,
      from: deployer,
      args: [ deployer ]
    });

    const factory = await ethers.getContractAt<IPangolinFactory>(
      'PangolinFactory', deployResult.address
    );

    const [ UST, USDC, USDCe ] = await Promise.all([
      ethers.getContract<IERC20Mock>('UST'),
      ethers.getContract<IERC20Mock>('USDC'),
      ethers.getContract<IERC20Mock>('USDC.e'),
    ]);

    const { AddressZero } = ethers.constants;

    // pair USDC.e/USDC
    try {
      const pairExists = await factory.getPair(USDC.address, USDCe.address);
      if(AddressZero === pairExists) {
        await factory.createPair(USDC.address, USDCe.address);
      }
    } catch {}

    // pair USDC/UST
    try {
      const pairExists = await factory.getPair(USDC.address, UST.address);
      if(AddressZero === pairExists) {
        await factory.createPair(USDC.address, UST.address);
      }
    } catch {}
  }
};

func.tags = ['PangolinFactory'];
func.dependencies = [
  'UST',
  'USDC',
  'USDC.e',
];

export default func;