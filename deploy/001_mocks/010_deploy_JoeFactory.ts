import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import {
  ERC20Mock as IERC20Mock,
  JoeFactory as IJoeFactory,
} from '../../build/types';

const func: DeployFunction = async function ( hre: HardhatRuntimeEnvironment ) {
  const { deployments, getNamedAccounts, ethers, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // The following script can only be run on networks with the tags 'local' and 'staging'
  if(network.tags.local || network.tags.staging) {
    // Contract used for testing purposes only
    const deployResult = await deploy('JoeFactory', {
      log: true,
      from: deployer,
      args: [ deployer ]
    });

    const factory = await ethers.getContractAt<IJoeFactory>('JoeFactory', deployResult.address);

    const [ USDC, USDCe ] = await Promise.all([
      ethers.getContract<IERC20Mock>('USDC'),
      ethers.getContract<IERC20Mock>('USDC.e'),
    ]);

    // pair USDC.e/USDC
    try {
      const pairExists = await factory.getPair(USDC.address, USDCe.address);
      if(ethers.constants.AddressZero === pairExists) {
        await factory.createPair(USDC.address, USDCe.address);
      }
    } catch {}
  }
};

func.tags = ['JoeFactory'];
func.dependencies = [
  'USDC',
  'USDC.e',
];

export default func;