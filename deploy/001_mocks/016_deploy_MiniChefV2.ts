import { Accounts } from '../../typescript/hardhat';
import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import {
  ERC20Mock as IERC20Mock,
  MiniChefV2 as IMiniChefV2,
  PangolinFactory as IPangolinFactory,
} from '../../build/types';

const func: DeployFunction = async function(hre: HardhatRuntimeEnvironment) {
  const { deployments, ethers, network, getNamedAccounts } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts() as Accounts;

  // The following script can only be run on networks with the tags 'local' and 'staging'
  if(network.tags.local || network.tags.staging) {

    const PNG = await ethers.getContract('PNG');

    /**
     * @description The data used in the constructor was replicated from the contract
     * deployed on mainnet. Contract used for testing purposes only
     * ref: https://snowtrace.io/address/0x5f69e5870f2C25cEA61622e932A46aDAbCFb3981#code
    **/
     const deployResult = await deploy('MiniChefV2', {
      log: true,
      from: deployer,
      args: [
        PNG.address, // address _rewardToken
        deployer, // address _firstOwner
      ]
    })

    const { AddressZero } = ethers.constants;
    const Chef = await ethers.getContractAt<IMiniChefV2>('MiniChefV2', deployResult.address);

    const [
      UST,
      USDC,
      USDCe,
      PangolinFactory,
    ] = await Promise.all([
      ethers.getContract<IERC20Mock>('UST'),
      ethers.getContract<IERC20Mock>('USDC'),
      ethers.getContract<IERC20Mock>('USDC.e'),
      ethers.getContract<IPangolinFactory>('PangolinFactory'),
    ]);

    const pairs = await Promise.all([
      PangolinFactory.getPair(USDC.address, USDCe.address), // USDC_USDCe - index: 0
      PangolinFactory.getPair(USDC.address, UST.address), // USDC_UST - index: 1
    ]);

    const USDC_USDCe = pairs[0];
    const USDC_UST = pairs[1];

    // The data used was replicated from the contract deployed on mainnet
    try { await Chef.addPool(500, USDC_USDCe, AddressZero); /* poolId: 0 */ }catch{}
    try { await Chef.addPool(500, USDC_UST, AddressZero); /* poolId: 1 */ }catch{}
  }
}

func.tags = ['MiniChefV2'];
func.dependencies = [
  'PNG',
  'UST',
  'USDC',
  'USDC.e',
  'PangolinFactory',
];

export default func;