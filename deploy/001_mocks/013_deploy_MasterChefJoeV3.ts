import { Accounts } from '../../typescript/hardhat';
import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { ERC20Mock as IERC20Mock } from '../../build/types/ERC20Mock';
import {
  JoeToken as IJoeToken,
  JoeFactory as IJoeFactory,
  MasterChefJoeV2 as IMasterChefJoeV2,
  MasterChefJoeV3 as IMasterChefJoeV3,
} from '../../build/types';

const func: DeployFunction = async function(hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, network, ethers } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts() as Accounts;

  const [
    USDC,
    USDCe,
    JoeToken,
    JoeFactory,
    MasterChefJoeV2,
  ] = await Promise.all([
    ethers.getContract<IERC20Mock>('USDC'),
    ethers.getContract<IERC20Mock>('USDC.e'),
    ethers.getContract<IJoeToken>('JoeToken'),
    ethers.getContract<IJoeFactory>('JoeFactory'),
    ethers.getContract<IMasterChefJoeV2>('MasterChefJoeV2'),
  ]);

  // The following script can only be run on networks with the tags 'local' and 'staging'
  if(network.tags.local || network.tags.staging) {
    const _MASTER_PID = 0; // see the MasterChefJoeV2 deployment file

    /**
     * @description The data used in the constructor was replicated from the contract
     * deployed on mainnet. Contract used for testing purposes only
     * ref: https://snowtrace.io/address/0x188bed1968b795d5c9022f6a0bb5931ac4c18f00
    **/
    const deployResult = await deploy('MasterChefJoeV3', {
      log: true,
      from: deployer,
      args: [
        MasterChefJoeV2.address, // IMasterChef _MASTER_CHEF_V2
        JoeToken.address, // IERC20 _joe
        _MASTER_PID, // uint256 _MASTER_PID
      ]
    })

    if(deployResult.newlyDeployed) {
      const MasterChef = await ethers.getContractAt<IMasterChefJoeV3>(
        'MasterChefJoeV3', deployResult.address
      );

      const pairs = await Promise.all([
        JoeFactory.getPair(USDC.address, USDCe.address), // USDC_USDCe - index: 0
      ]);

      const USDC_USDCe = pairs[0];

      // The data used was replicated from the contract deployed on mainnet
      try {
        await MasterChef.add(0, USDC_USDCe, ethers.constants.AddressZero); /* poolId: 0 */
      } catch{}
    }
  }
}

func.tags = ['MasterChefJoeV3'];
func.dependencies = [
  'USDC',
  'USDC.e',
  'JoeToken',
  'JoeFactory',
  'MasterChefJoeV2'
];

export default func;