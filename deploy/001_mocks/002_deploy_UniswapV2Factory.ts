import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import {
  ERC20Mock as IERC20,
  WrappedFtm as IWrappedFtm,
  SpookyToken as ISpookyToken,
  UniswapV2Factory as IUniswapV2Factory,
} from '../../build/types';

const func: DeployFunction = async function ( hre: HardhatRuntimeEnvironment ) {
  const { deployments, getNamedAccounts, network, ethers } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // The following script can only be run on networks with the tags 'local'
  if(network.tags.local) {
    // Contract used for testing purposes only
    const deployResult = await deploy('UniswapV2Factory', {
      log: true,
      from: deployer,
      args: [ deployer ]
    });

    const UniswapV2Factory = await ethers.getContractAt(
      'UniswapV2Factory', deployResult.address
    ) as IUniswapV2Factory;

    const [ USDC, WrappedFtm, SpookyToken ] = await Promise.all([
      ethers.getContract<IERC20>('USDC'),
      ethers.getContract<IWrappedFtm>('WrappedFtm'),
      ethers.getContract<ISpookyToken>('SpookyToken'),
    ]);

    // pair USDC/WFTM
    {
      const pairExists = await UniswapV2Factory.getPair(USDC.address, WrappedFtm.address);
      if(ethers.constants.AddressZero === pairExists) {
        await UniswapV2Factory.createPair(USDC.address, WrappedFtm.address);
      }
    }
    // pair BOO/WFTM
    {
      const pairExists = await UniswapV2Factory.getPair(SpookyToken.address, WrappedFtm.address);
      if(ethers.constants.AddressZero === pairExists) {
        await UniswapV2Factory.createPair(SpookyToken.address, WrappedFtm.address);
      }
    }
  }
};

func.tags = ['UniswapV2Factory'];
func.dependencies = [
  'USDC',
  'WrappedFtm',
  'SpookyToken',
];

export default func;