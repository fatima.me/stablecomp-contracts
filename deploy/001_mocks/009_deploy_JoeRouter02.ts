import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';

const func: DeployFunction = async function ( hre: HardhatRuntimeEnvironment ) {
  const { deployments, getNamedAccounts, ethers, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // The following script can only be run on networks with the tags 'local' and 'staging'
  if(network.tags.local || network.tags.staging) {
    const [ WAVAX, JoeFactory ] = await Promise.all([
      ethers.getContract('WAVAX'),
      ethers.getContract('JoeFactory')
    ])

    // Contract used for testing purposes only
    await deploy('JoeRouter02Mock', {
      log: true,
      from: deployer,
      args: [
        JoeFactory.address,
        WAVAX.address,
      ]
    });
  }
};

func.tags = ['JoeRouter02Mock'];
func.dependencies = [
  'WAVAX',
  'JoeFactory',
];

export default func;