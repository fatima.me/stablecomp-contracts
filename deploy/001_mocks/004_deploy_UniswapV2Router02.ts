import { parseEther } from 'ethers/lib/utils';
import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import {
  ERC20 as IERC20,
  UniswapV2Router02 as IUniswapV2Router02
} from '../../build/types';

const func: DeployFunction = async function ( hre: HardhatRuntimeEnvironment ) {
  const { deployments, getNamedAccounts, ethers, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // The following script can only be run on networks with the tags 'local'
  if(network.tags.local) {
    const [ WETH, UniswapV2Factory ] = await Promise.all([
      ethers.getContract('WrappedFtm'),
      ethers.getContract('UniswapV2Factory')
    ])

    // Contract used for testing purposes only
    const deployResult = await deploy('UniswapV2Router02', {
      log: true,
      from: deployer,
      args: [
        UniswapV2Factory.address,
        WETH.address,
      ]
    });

    const UniswapV2Router02 = await ethers.getContractAt(
      'UniswapV2Router02', deployResult.address
    ) as IUniswapV2Router02;

    const USDC = await ethers.getContract<IERC20>('USDC')

    // approve the amount of token I want to delegate to add liquidity
    const amountETHDesired = parseEther('900');
    const amountTokenDesired = parseEther('1188');
    const deadlineAddLiquidity = +new Date();
    await USDC.approve(UniswapV2Router02.address, amountTokenDesired);

    const balance = await ethers.provider.getBalance(deployer);

    // If the balance to add to liquidity is not enough, throw a warning
    if(balance.lt(amountETHDesired)) {
      console.log(
        `\nYou do not have enough balance to add liquidity, you will need to do ` +
        `it manually after deployment\n`
      );
    } else {
      // Liquidity is added to the USDC/FTM pair in a proportion so that the price of the FTM
      // is 1.32 USDC. FTM price as of 03/24/2022
      // ftm_price = usdc_liquidity_pool / ftm_liquidity_pool
      // 1.32 = 1188 / 900
      await UniswapV2Router02.addLiquidityETH(
        USDC.address,
        amountTokenDesired,
        amountTokenDesired,
        amountETHDesired,
        deployer,
        deadlineAddLiquidity,
        { value: amountETHDesired }
      );
    }
  }
};

func.tags = ['UniswapV2Router02'];
func.dependencies = [
  'USDC',
  'UniswapV2Factory'
];

export default func;