import { Accounts } from '../../typescript/hardhat';
import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';

const func: DeployFunction = async function(hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts() as Accounts;

  // The following script can only be run on networks with the tags 'local' and 'staging'
  if(network.tags.local || network.tags.staging) {
    // Contract used for testing purposes only
    await deploy('USDC', {
      log: true,
      from: deployer,
      args: [
        'Stablecomp USDC', // string memory name_
        'USDC', // string memory symbol_
      ],
      contract: 'ERC20Mock'
    })
  }
}

func.tags = ['USDC'];

export default func;