import { Accounts } from '../../typescript/hardhat';
import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { MasterChef as IMasterChef } from '../../build/types/MasterChef';
import {
  ERC20Mock as IERC20,
  WrappedFtm as IWrappedFtm,
  SpookyToken as ISpookyToken,
  UniswapV2Factory as IUniswapV2Factory,
} from '../../build/types';

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, artifacts, ethers, getNamedAccounts, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts() as Accounts;

  // The following script can only be run on networks with the tags 'local'
  if(network.tags.local) {
    const MasterChefArtifact = await artifacts.readArtifact(
      'contracts/thirdPartyContracts/SpookySwap/MasterChef.sol:MasterChef'
    );

    const [
      USDC,
      WrappedFtm,
      SpookyToken,
      UniswapV2Factory,
    ] = await Promise.all([
      ethers.getContract<IERC20>('USDC'),
      ethers.getContract<IWrappedFtm>('WrappedFtm'),
      ethers.getContract<ISpookyToken>('SpookyToken'),
      ethers.getContract<IUniswapV2Factory>('UniswapV2Factory'),
    ]);

    /**
     * @description The data used in the constructor was replicated from the contract
     * deployed on mainnet. Contract used for testing purposes only
     * ref: https://ftmscan.com/address/0x2b2929e785374c651a81a63878ab22742656dcdd#readContract
    **/
    const deployResult = await deploy('MasterChef', {
      log: true,
      from: deployer,
      contract: MasterChefArtifact,
      args: [
        SpookyToken.address, // SpookyToken _boo
        deployer, // address _devaddr
        '116981621400924000', // uint256 _booPerSecond
        '1593520884', // uint256 _startTime
      ]
    });

    const MasterChef = await ethers.getContractAt(
      'MasterChef', deployResult.address
    ) as IMasterChef;

    if(deployResult.newlyDeployed) {
      // grant ownership of BOO token to MasterChef to follow mainnet flow
      await SpookyToken.transferOwnership(MasterChef.address);
    }

    const pairs = await Promise.all([
      UniswapV2Factory.getPair(SpookyToken.address, WrappedFtm.address), // BOO_WFTM - index: 0
      UniswapV2Factory.getPair(USDC.address, WrappedFtm.address), // USDC_WFTM - index: 1
      UniswapV2Factory.getPair(USDC.address, SpookyToken.address), // USDC_BOO - index: 2
    ]);

    const PAIR_BOO_WFTM = pairs[0];
    const PAIR_USDC_WFTM = pairs[1];
    const PAIR_USDC_BOO = pairs[2];

    // The data used was replicated from the contract deployed on mainnet
    try { await MasterChef.add(3000, PAIR_BOO_WFTM); /* poolId: 0 */ } catch{}
    try { await MasterChef.add(900, PAIR_USDC_WFTM); /* poolId: 1 */ } catch{}
    try { await MasterChef.add(3000, PAIR_USDC_BOO); /* poolId: 2 */ } catch{}
  }
}

func.tags = ['MasterChefSpookySwap'];
func.dependencies = [
  'USDC',
  'WrappedFtm',
  'SpookyToken',
  'UniswapV2Factory',
];

export default func;