import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';

const func: DeployFunction = async function ( hre: HardhatRuntimeEnvironment ) {
  const { deployments, getNamedAccounts, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // The following script can only be run on networks with the tags 'local' or 'staging'
  if(network.tags.local || network.tags.staging) {
    // Contract used for testing purposes only
    await deploy('WAVAX', {
      log: true,
      from: deployer,
    });
  }
};

export default func;

func.tags = ['WAVAX'];
