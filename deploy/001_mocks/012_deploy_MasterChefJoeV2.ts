import { Accounts } from '../../typescript/hardhat';
import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { ERC20Mock as IERC20Mock } from '../../build/types/ERC20Mock';
import {
  JoeToken as IJoeToken,
  MasterChefJoeV2 as IMasterChefJoeV2
} from '../../build/types';

const func: DeployFunction = async function(hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, network, ethers } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts() as Accounts;

  const [
    JoeToken,
    JoeDummyToken,
  ] = await Promise.all([
    ethers.getContract<IJoeToken>('JoeToken'),
    ethers.getContract<IERC20Mock>('JoeDummyToken'),
  ]);

  // The following script can only be run on networks with the tags 'local' and 'staging'
  if(network.tags.local || network.tags.staging) {
    /**
     * @description The data used in the constructor was replicated from the contract
     * deployed on mainnet. Contract used for testing purposes only
     * ref: https://snowtrace.io/address/0xd6a4f121ca35509af06a0be99093d08462f53052
    **/
    const deployResult = await deploy('MasterChefJoeV2', {
      log: true,
      from: deployer,
      args: [
        JoeToken.address, // JoeToken _joe
        deployer, // address _devAddr
        deployer, // address _treasuryAddr
        deployer, // address _investorAddr
        '7500000000000000000', // uint256 _joePerSec
        1651255236582, // uint256 _startTimestamp
        200, // uint256 _devPercent
        200, // uint256 _treasuryPercent
        100, // uint256 _investorPercent
      ]
    })

    const MasterChef = await ethers.getContractAt<IMasterChefJoeV2>(
      'MasterChefJoeV2', deployResult.address
    );

    if(deployResult.newlyDeployed) {
      // grant ownership of JOE token to MasterChef to follow mainnet flow
      await JoeToken.transferOwnership(MasterChef.address);
    }

    // The data used was replicated from the contract deployed on mainnet
    try {
      await MasterChef.add(
        3650, JoeDummyToken.address, ethers.constants.AddressZero
      ); /* poolId: 0 */
    } catch{}
  }
}

func.tags = ['MasterChefJoeV2'];
func.dependencies = [
  'JoeToken',
  'JoeDummyToken',
];

export default func;