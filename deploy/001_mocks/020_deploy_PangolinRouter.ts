import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';

const func: DeployFunction = async function ( hre: HardhatRuntimeEnvironment ) {
  const { deployments, getNamedAccounts, ethers, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts();

  // The following script can only be run on networks with the tags 'local' and 'staging'
  if(network.tags.local || network.tags.staging) {
    const [ WAVAX, PangolinFactory ] = await Promise.all([
      ethers.getContract('WAVAX'),
      ethers.getContract('PangolinFactory')
    ])

    // Contract used for testing purposes only
    await deploy('PangolinRouter', {
      log: true,
      from: deployer,
      args: [
        PangolinFactory.address,
        WAVAX.address,
      ]
    });
  }
};

func.tags = ['PangolinRouter'];
func.dependencies = [
  'WAVAX',
  'PangolinFactory',
];

export default func;