import { Accounts } from '../../typescript/hardhat';
import { DeployFunction } from 'hardhat-deploy/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';

const func: DeployFunction = async function(hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, network } = hre;
  const { deploy } = deployments;
  const { deployer } = await getNamedAccounts() as Accounts;

  // The following script can only be run on networks with the tags 'local'
  if(network.tags.local) {
    // Contract used for testing purposes only
    await deploy('SpookyToken', {
      log: true,
      from: deployer,
      args: []
    })
  }
}

func.tags = ['SpookyToken'];

export default func;