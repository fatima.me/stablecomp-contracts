import { DeployFunction } from 'hardhat-deploy/dist/types';
import { HardhatRuntimeEnvironment } from 'hardhat/types';
import { Accounts } from '../typescript/hardhat';

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, ethers, getNamedAccounts, network } = hre;
  const { deploy } = deployments;
  const { deployer } = (await getNamedAccounts()) as Accounts;

  // The following script can only be run on networks with the tags 'local'
  if(network.tags.local) {
    const SCOMP = await ethers.getContract('SCOMP');
  
    await deploy('TokenVesting', {
      log: true,
      from: deployer,
      args: [ SCOMP.address ]
    })
  }
}

func.tags = ['TokenVesting'];
func.dependencies = ['SCOMP'];

export default func;