import { task } from 'hardhat/config';
import { HardhatRuntimeEnvironment, TaskArguments } from "hardhat/types";

task('signers')
.setAction(async (taskArguments: TaskArguments, hre: HardhatRuntimeEnvironment) => {
  const { ethers, getNamedAccounts } = hre;
  const signers = await ethers.getNamedSigners()
  console.log('signers', signers);
})

