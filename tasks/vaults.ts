import { task } from 'hardhat/config';
import { avaxfujiVaults } from '../addressbook/vaults/avaxfujiVaults';

/* types */
import {
  StrategyDualLP,
  JoePair as IJoePair,
  ERC20Detailed as IERC20,
  StablecompVaultV1 as IStablecompVaultV1,
} from '../build/types';
import type { IVaultInfoTasks } from '../typescript/vaults';
import type { HardhatRuntimeEnvironment } from "hardhat/types";

interface checkVaultArguments extends IVaultInfoTasks {}

async function getVaultsInfo(vaults: IVaultInfoTasks[], hre: HardhatRuntimeEnvironment) {
  const { ethers } = hre;
  const result: any[] = [];

  for(const index in vaults) {
    const vaultData = vaults[index];
    const { vault, oneClick } = vaultData;
    
    const vaultContract = await ethers.getContractAt<IStablecompVaultV1>(
      'StablecompVaultV1', vault
    );

    const [
      earnedTokenName,
      earnedTokenSymbol,
      earnedTokenDecimals,
      strategy,
    ] = await Promise.all([
      vaultContract.name(),
      vaultContract.symbol(),
      vaultContract.decimals(),
      vaultContract.strategy(),
    ]);

    const strategyContract = await ethers.getContractAt<StrategyDualLP>(
      'StrategyDualLP', strategy
    );

    const [
      pool,
      poolId,
      masterChef,
    ] = await Promise.all([
      strategyContract.want(),
      strategyContract.poolId(),
      strategyContract.masterchef(),
    ]);

    const LPToken = await ethers.getContractAt<IJoePair>('JoePair', pool);

    const [
      lpName,
      lpSymbol,
      lpDecimals,
      lpToken0,
      lpToken1,
    ] = await Promise.all([
      LPToken.name(),
      LPToken.symbol(),
      LPToken.decimals(),
      LPToken.token0(),
      LPToken.token1(),
    ]);

    const ERC20ArtifactName = '@openzeppelin/contracts/token/ERC20/ERC20.sol:ERC20';

    const [
      lpToken0Contract,
      lpToken1Contract,
    ] = await Promise.all([
      ethers.getContractAt<IERC20>(ERC20ArtifactName, lpToken0),
      ethers.getContractAt<IERC20>(ERC20ArtifactName, lpToken1)
    ]);

    const [
      lpToken0Name,
      lpToken0Symbol,
      lpToken0Decimals,
      lpToken1Name,
      lpToken1Symbol,
      lpToken1Decimals,
    ] = await Promise.all([
      lpToken0Contract.name(),
      lpToken0Contract.symbol(),
      lpToken0Contract.decimals(),
      lpToken1Contract.name(),
      lpToken1Contract.symbol(),
      lpToken1Contract.decimals(),
    ])

    result.push({
      address: vault,
      depositsPaused: false,
      strategy,
      risks: [],
      oneClicAddress: oneClick,
      masterChef,
      pool: {
        poolId: poolId.toNumber(),
        name: lpName,
        symbol: lpSymbol,
        address: pool,
        decimals: lpDecimals,
        lp0: {
          address: lpToken0,
          oracle: 'tokens',
          oracleId: lpToken0Symbol,
          decimals: lpToken0Decimals,
          tokenName: lpToken0Name,
          tokenSymbol: lpToken0Symbol,
          tokenDescription: '',
        },
        lp1: {
          address: lpToken1,
          oracle: 'tokens',
          oracleId: lpToken1Symbol,
          decimals: lpToken1Decimals,
          tokenName: lpToken1Name,
          tokenSymbol: lpToken1Symbol,
          tokenDescription: '',
        },
      },
      earnedToken: {
        address: vault,
        tokenName: earnedTokenName,
        decimals: earnedTokenDecimals,
        tokenSymbol: earnedTokenSymbol,
      }
    });
  }

  return result;
}

task('getVaultInfo')
.addParam('vault', 'Address of vault contract')
.addParam('oneClick', 'Address of OneClick contract')
.setAction(async (taskArguments: checkVaultArguments, hre: HardhatRuntimeEnvironment) => {
  const { vault, oneClick } = taskArguments;

  const vaultInfo = await getVaultsInfo([{ vault, oneClick }], hre);

  console.log('Vault info:', JSON.stringify(vaultInfo[0], null, 4));
})

task('getVaultsInfoByNetwork')
.setAction(async (_, hre: HardhatRuntimeEnvironment) => {
  const { network } = hre;

  const adressbook: Record<string, IVaultInfoTasks[]> = {
    fuji: avaxfujiVaults,
  }

  const vaults = adressbook[network.name];

  if(!vaults) {
    console.log(`There are no vaults for the ${network.name} network`);
    return;
  }

  const vaultsInfo = await getVaultsInfo(vaults, hre);

  console.log(JSON.stringify(vaultsInfo, null, 4));
})