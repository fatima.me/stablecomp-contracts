/// Copyright (c) 2022 Stablecomp ltd.
/// opensource@stablecomp.com
/// SPDX-License-Identifier: MIT
/// Licensed under the MIT License;
/// You may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
/// https://github.com/stablecomp/contracts/blob/main/LICENSE
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.

pragma solidity 0.8.8;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@uniswap/v3-core/contracts/libraries/LowGasSafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";

import "../interfaces/IStableSwap3Pool.sol";
import "../interfaces/IStablecompVaultV1.sol";
import "../interfaces/IStrategyCurve3Pool.sol";
import { TokenData } from "../strategies/curve/structs.sol";

contract OneClickCurveLPV1 is OwnableUpgradeable, UUPSUpgradeable {
	using SafeERC20 for IERC20;
	using LowGasSafeMath for uint256;
	using SafeERC20 for IStablecompVaultV1;

	address public wrapped; // Wrapped Native Token. Example: Wrapped ETH (WETH).
	uint256 public constant minimumAmount = 1000;

	function initialize(address _wrapped) external initializer {
		__Ownable_init();
		__UUPSUpgradeable_init();

		wrapped = _wrapped;
	}

	receive() external payable {
		assert(msg.sender == wrapped);
	}

	/**
	 * @dev Starting point for auto composition.
	 * @param _vault Address of the vault that controls the funds of the strategy.
	 * @param _strategy Address of the strategy.
	 * @param _tokensIn Addresses of the tokens to deposit.
	 * @param _tokensInAmount Amounts of tokens to deposit.
	**/
	function scompIn(
		address _vault,
		address _strategy,
		address[] calldata _tokensIn,
		uint256[] calldata _tokensInAmount
	) external {
		IStablecompVaultV1 vault = IStablecompVaultV1(_vault);
		require(_strategy == vault.strategy(), "Incompatible strategy");

		IStrategyCurve3Pool strategy = IStrategyCurve3Pool(_strategy);

		uint256 poolSize = strategy.poolSize();

		require(
			_tokensIn.length == poolSize && _tokensInAmount.length == poolSize,
			"The data provided does not match the size of the pool"
		);

		IStableSwap3Pool pool = IStableSwap3Pool(strategy.pool());

		// Amounts to deposit in the pool.
		uint256[3] memory amounts;

		for(uint256 i; i < poolSize; i++) {
			address _t = _tokensIn[i];
			if(_t == address(0)) continue;

			TokenData memory _tokenData = strategy.tokensInThePool(_t);

			require(_tokenData.exists, "Token does not exist in the pool");

			amounts[_tokenData.index] = _tokensInAmount[i];

			require(
				IERC20(_t).allowance(msg.sender, address(this)) >= _tokensInAmount[i],
				"Input token is not approved"
			);

			// Assign the tokens to this contract.
			IERC20(_t).safeTransferFrom(msg.sender, address(this), _tokensInAmount[i]);

			_approveTokenIfNeeded(_t, address(pool));
		}

		address pair = vault.want();
		pool.add_liquidity(amounts, 0);

		uint256 amountLiquidity = IERC20(pair).balanceOf(address(this));
		_approveTokenIfNeeded(address(pair), address(vault));

		vault.deposit(amountLiquidity);
		vault.safeTransfer(msg.sender, vault.balanceOf(address(this)));
	}

	function _authorizeUpgrade(address newImplementation) internal override onlyOwner {}

	function _approveTokenIfNeeded(address token, address spender) private {
		if(IERC20(token).allowance(address(this), spender) == 0) {
			IERC20(token).safeApprove(spender, type(uint256).max);
		}
	}
}
