// SPDX-License-Identifier: MIT
/// @dev Contract used for testing purposes only

pragma solidity 0.8.8;

import "../../mocks/tokens/ERC20Mock.sol";

contract PNG is ERC20Mock {

  constructor(string memory name_, string memory symbol_) ERC20Mock(name_, symbol_) {}

}