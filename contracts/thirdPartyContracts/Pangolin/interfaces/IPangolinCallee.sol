// SPDX-License-Identifier: MIT
/// @dev Contract used for testing purposes only

pragma solidity >=0.5.0;

interface IPangolinCallee {
  function pangolinCall(
    address sender,
    uint256 amount0,
    uint256 amount1,
    bytes calldata data
  ) external;
}
