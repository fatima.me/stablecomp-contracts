// SPDX-License-Identifier: MIT
/// @dev Contract used for testing purposes only

pragma solidity =0.6.12;

import "@joe-defi/core/contracts/traderjoe/JoeFactory.sol";