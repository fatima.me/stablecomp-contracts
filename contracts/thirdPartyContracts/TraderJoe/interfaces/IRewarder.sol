// SPDX-License-Identifier: MIT
/// @dev Contract used for testing purposes only

pragma solidity 0.8.8;

interface IRewarder {
    function onJoeReward(address user, uint256 newLpAmount) external;

    function pendingTokens(address user)
        external
        view
        returns (uint256 pending);

    function rewardToken() external view returns (address);
}