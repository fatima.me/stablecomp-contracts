// SPDX-License-Identifier: MIT
/// @dev Contract used for testing purposes only

pragma solidity =0.7.6;

contract UniswapV3Factory {
    mapping(address => mapping(address => mapping(uint24 => address))) public getPool;

    constructor() {}
}