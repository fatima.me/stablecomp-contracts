/// Copyright (c) 2022 Stablecomp ltd.
/// opensource@stablecomp.com
/// SPDX-License-Identifier: MIT
/// Licensed under the MIT License;
/// You may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
/// https://github.com/stablecomp/contracts/blob/main/LICENSE
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.

pragma solidity 0.8.8;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

import "hardhat/console.sol";

/**
 * @dev A token holder contract that will allow a beneficiary to extract the tokens
 * after a give release time. The same beneficiaries can be granted the possibility
 * of multiple allocations at different time cliff.
 *
 * Any token transferred to this contract will follow the vesting schedule as if they
 * were locked from the beginning. Consequently, if the vesting has already started,
 * any amount of tokens sent to this contract will (at least partly) be immediately releasable.
**/
contract TokenVesting {
	using SafeMath for uint256;
	using SafeERC20 for IERC20;

	IERC20 token;

	struct LockBox {
		uint256 balance; // Fully vested amount
		uint256 releaseTime; // Timestamp when token release is enabled
		address beneficiary; // Beneficiary of tokens after they are released
	}

	/// @dev This could be a mapping by address, but these numbered lockBoxes support possibility
	/// of multiple tranches per address
	LockBox[] public lockedBoxes;

	/// @dev Withdrawal record in locked boxes
	event LogLockBoxWithdrawal(uint lockBoxId, address receiver, uint256 amount);

	/// @dev Deposit record in locked boxes
	event LogLockBoxDeposit(
		uint lockBoxId,
		uint256 amount,
		uint256 releaseTime,
		address beneficiary
	);

	constructor(IERC20 tokenContract) {
		token = IERC20(tokenContract);
	}

	/**
	 * @notice Deposit tokens to be claimed by a beneficiary in the future
	 * @param amount Fully vested amount
	 * @param releaseTime Time when the tokens are released in seconds since Unix epoch
	 * (i.e. Unix timestamp). If not specified, vesting begins as soon as the transaction
	 * is mined
	 * @param beneficiary beneficiary of tokens after they are released
	**/
	function deposit(
		uint256 amount,
		uint256 releaseTime,
		address beneficiary
	) public {
		require(amount > 0, "Vested amount must be greater than zero");
		require(beneficiary != address(0), "Beneficiary cannot be zero address");

		if(releaseTime == 0) {
			releaseTime = block.timestamp;
		}

		lockedBoxes.push(LockBox(amount, releaseTime, beneficiary));

		token.safeTransferFrom(msg.sender, address(this), amount);

		emit LogLockBoxDeposit(lockedBoxes.length - 1, amount, releaseTime, beneficiary);
	}

	function withdraw(uint256 lockBoxNumber) public {
		LockBox storage l = lockedBoxes[lockBoxNumber];
		require(l.beneficiary == msg.sender, "Unauthorized address");
		require(l.releaseTime <= block.timestamp, "Tokens are still locked");
		uint256 amount = l.balance;
		delete l.balance;

		token.safeTransfer(msg.sender, amount);
		emit LogLockBoxWithdrawal(lockBoxNumber, msg.sender, amount);
	}

	/**
	 * @dev Releasable amount of tokens. In case the tokens are still blocked,
	 * the return value will be 0
	 * @param lockBoxNumber Lock box identifier 'ID'
	 **/
	function getLockBoxData(uint256 lockBoxNumber) public view returns (
		uint256 balance,
		uint256 releaseTime,
		address beneficiary
	) {
		return (
			lockedBoxes[lockBoxNumber].balance,
			lockedBoxes[lockBoxNumber].releaseTime,
			lockedBoxes[lockBoxNumber].beneficiary
		);
	}

	/// @dev Get number of lock box
	function getLockBoxCount() public view returns(uint) {
		return lockedBoxes.length;
	}
}
