/// Copyright (c) 2022 Stablecomp ltd.
/// opensource@stablecomp.com
/// SPDX-License-Identifier: MIT
/// Licensed under the MIT License;
/// You may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
/// https://github.com/stablecomp/contracts/blob/main/LICENSE
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.

pragma solidity 0.8.8;

import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";

import "../common/FeeManager.sol";
import "../common/StratManager.sol";
import { TokenData } from "./structs.sol";
import "../../interfaces/IGaugeFactory.sol";
import "../../interfaces/ILiquidityGauge.sol";
import "../../interfaces/IStableSwap3Pool.sol";

import "hardhat/console.sol";

contract StrategyCurve3Pool is StratManager, FeeManager, UUPSUpgradeable {
  using SafeMath for uint256;
  using SafeERC20 for IERC20;

  // Tokens used
  address public want; // LP token we want. Example: Curve Liquidity (3CRV).
  address public output; // Governance token obtained by the strategy. Example: Curve (CRV).
  address public wrapped; // Wrapped Native Token. Example: Wrapped ETH (WETH).
  address public depositToken; // Token used to add liquidity.

  // Third party contracts
  uint256 public poolSize;
  uint public depositIndex; // Index in the pool of the token to deposit.
  address public gaugeFactory;
  IStableSwap3Pool public pool;
  address public liquidityGauge; // Measures liquidity provided by users over time, in order to
  // distribute CRV and other rewards.

  // Tokens that are present in the pool.
  // Address token => TokenData
  mapping(address => TokenData) public tokensInThePool;

  // Routes
  address[] public outputToWrappedRoute;
  address[] public wrappedToDepositRoute;

  /// @dev Event that is fired each time someone harvests to the strat.
  event StratHarvest(address indexed harvester, uint256 wantHarvested, uint256 tvl);
  /// @dev Event that is fired each time someone deposit funds to the strat.
  event Deposit(uint256 tvl);
  /// @dev Event that is fired each time someone withdraw funds to the strat.
  event Withdraw(uint256 tvl);

  /**
   * @dev Configure and start the strategy
   * @param _want LP token we want. Example: Pangolin Liquidity (PGL).
   * @param _vault Address of the vault that controls the funds strategy.
   * @param _pool Address of pool to deposit funds.
   * @param _liquidityGauge Address of liquidityGauge to distribute CRV and other rewards.
   * @param _keeper Address to use as alternative owner.
   * @param _unirouter Router to use for swaps.
   * @param _strategist Address where strategist fees go.
   * @param _treasuryFeeRecipient Address where to send treasury fees.
   * @param _outputToWrappedRoute An array of token addresses to swap the output token for the
   * @param _wrappedToDepositRoute An array of token addresses to swap the wrapped native token
   * for the deposit  token
   **/
  function initialize(
    address _want,
    address _vault,
    address _pool,
    address _liquidityGauge,
    address _keeper,
    address _unirouter,
    address _strategist,
    address _treasuryFeeRecipient,
    address[] memory _outputToWrappedRoute,
    address[] memory _wrappedToDepositRoute
  ) external initializer {
    __StratManager_init(_keeper, _strategist, _unirouter, _vault, _treasuryFeeRecipient);
    __UUPSUpgradeable_init();

    want = _want;
    pool = IStableSwap3Pool(_pool);
    liquidityGauge = _liquidityGauge;
    output = _outputToWrappedRoute[0];
    wrapped = _outputToWrappedRoute[_outputToWrappedRoute.length - 1];
    depositToken = _wrappedToDepositRoute[_wrappedToDepositRoute.length - 1];

    uint256 _poolSize = 0;
    bool foundIndex = false;
    bool fetchPoolSize = true;

    // Calculate pool size.
    while(fetchPoolSize) {
      try pool.coins(_poolSize) returns(address _token) {
        if(_token == depositToken) {
          foundIndex = true;
          depositIndex = _poolSize;
        }

        tokensInThePool[_token] = TokenData({
          exists: true,
          index: _poolSize
        });
        _poolSize++;
      } catch {
        // When an error occurs it is because the pool size was found.
        fetchPoolSize = false;
      }
    }

    poolSize = _poolSize;
    require(foundIndex, "Index in the pool of the token to deposit not found");

    _giveAllowances();
  }

  /// @dev Throws if called by any account other than the vault.
  modifier onlyVault() {
    require(msg.sender == vault, "Ownable: caller is not the vault");
    _;
  }

  /// @dev compounds earnings and charges performance fee
  function harvest() external whenNotPaused {
    if(gaugeFactory != address(0)) {
      IGaugeFactory(gaugeFactory).mint(liquidityGauge);
    }
    ILiquidityGauge(liquidityGauge).claim_rewards(address(this));
    swapRewardsToNative();
    uint256 nativeBal = IERC20(wrapped).balanceOf(address(this));
    if(nativeBal > 0) {
      chargeFees();
      addLiquidity();
      uint256 wantHarvested = balanceOfWant();
      deposit();
      emit StratHarvest(msg.sender, wantHarvested, balanceOf());
    }
  }

  /// @dev Withdraw the amount `_amount` from the funds deposited
  function withdraw(uint256 _amount) external onlyVault {
    uint256 wantBal = IERC20(want).balanceOf(address(this));

    if(wantBal < _amount) {
      ILiquidityGauge(liquidityGauge).withdraw(_amount.sub(wantBal));
      wantBal = IERC20(want).balanceOf(address(this));
    }

    if(wantBal > _amount) {
      wantBal = _amount;
    }

    if(tx.origin == owner() || paused()) {
      IERC20(want).safeTransfer(vault, wantBal);
    } else {
      uint256 withdrawalFeeAmount = wantBal.mul(withdrawalFee).div(PERCENTAGE_DENOMINATOR);
      IERC20(want).safeTransfer(vault, wantBal.sub(withdrawalFeeAmount));
    }

    emit Withdraw(balanceOf());
  }

  /// @dev Called as part of strat migration. Sends all the available funds back to the vault.
  function retireStrat() external onlyVault {
    ILiquidityGauge(liquidityGauge).withdraw(balanceOfPool());

    uint256 wantBal = IERC20(want).balanceOf(address(this));
    IERC20(want).transfer(vault, wantBal);
  }

  /// @dev Puts the funds to work.
  function deposit() public whenNotPaused {
    uint256 wantBal = IERC20(want).balanceOf(address(this));

    if(wantBal > 0) {
      ILiquidityGauge(liquidityGauge).deposit(wantBal);
      emit Deposit(balanceOf());
    }
  }

  /// @dev Calculate the total underlaying 'want' held by the strat.
  function balanceOf() public view returns (uint256) {
    return balanceOfWant().add(balanceOfPool());
  }

  /// @dev It calculates how much 'want' this contract holds.
  function balanceOfWant() public view returns (uint256) {
    return IERC20(want).balanceOf(address(this));
  }

  /// @dev It calculates how much 'want' the strategy has working in the farm.
  function balanceOfPool() public view returns (uint256) {
    return ILiquidityGauge(liquidityGauge).balanceOf(address(this));
  }

  function swapRewardsToNative() internal {
    uint256 outputBal = IERC20(output).balanceOf(address(this));

    if(outputBal > 0) {
      IUniswapV2Router02(unirouter).swapExactTokensForTokens(
        outputBal, 0, outputToWrappedRoute, address(this), block.timestamp
      );
    }
  }

  /// @dev performance fees
	function chargeFees() internal {
    uint256 wrappedBal = IERC20(wrapped)
      .balanceOf(address(this)).mul(PERFORMANCE_FEE).div(PERCENTAGE_DENOMINATOR);

    uint256 govFeeAmount = wrappedBal.mul(GOVERNANCE_POOL_FEE).div(PERCENTAGE_DENOMINATOR);
    IERC20(wrapped).safeTransfer(owner(), govFeeAmount);

    uint256 treasuryFeeAmount = wrappedBal.mul(TREASURY_FEE).div(PERCENTAGE_DENOMINATOR);
    IERC20(wrapped).safeTransfer(treasuryFeeRecipient, treasuryFeeAmount);

    uint256 strategistFee = wrappedBal.mul(STRATEGIST_FEE).div(PERCENTAGE_DENOMINATOR);
    IERC20(wrapped).safeTransfer(strategist, strategistFee);

    uint256 callFeeAmount = wrappedBal.mul(HARVEST_CALL_FEE).div(PERCENTAGE_DENOMINATOR);
    IERC20(wrapped).safeTransfer(tx.origin, callFeeAmount);

    // TODO: Define to which address BURNED_FEE is sent
  }

  /// @dev Adds liquidity to AMM and gets more LP tokens.
	function addLiquidity() internal {
    uint256 wrappedBal = IERC20(wrapped).balanceOf(address(this));

    IUniswapV2Router02(unirouter).swapExactTokensForTokens(
      wrappedBal, 0, wrappedToDepositRoute, address(this), block.timestamp
    );

    uint256[3] memory amounts;
    amounts[depositIndex] = IERC20(depositToken).balanceOf(address(this));

    IStableSwap3Pool(pool).add_liquidity(amounts, 0);
  }

  function _giveAllowances() internal {
    IERC20(output).safeApprove(unirouter, type(uint256).max);
    IERC20(want).safeApprove(liquidityGauge, type(uint256).max);
  }

  function _removeAllowances() internal {
    IERC20(output).safeApprove(unirouter, 0);
    IERC20(want).safeApprove(liquidityGauge, 0);
  }

  function _authorizeUpgrade(address) internal override onlyOwner {}
}
