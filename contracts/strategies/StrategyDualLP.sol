/// Copyright (c) 2022 Stablecomp ltd.
/// opensource@stablecomp.com
/// SPDX-License-Identifier: MIT
/// Licensed under the MIT License;
/// You may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
/// https://github.com/stablecomp/contracts/blob/main/LICENSE
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.

pragma solidity 0.8.8;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@uniswap/v2-periphery/contracts/interfaces/IWETH.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol";
import "@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";

import "./common/FeeManager.sol";
import "./common/StratManager.sol";
import "../interfaces/IMasterChef.sol";

contract StrategyDualLP is StratManager, FeeManager, UUPSUpgradeable {
  using SafeMath for uint256;
  using SafeERC20 for IERC20;

  // Tokens used
  address public want; // LP token we want. example: Joe LP Token (JLP)
  address public output; // Governance token obtained by the strategy. example: JoeToken (JOE)
  address public wrapped; // Wrapped Native Token. Example: Wrapped AVAX (WAVAX)
  address public lpToken0;
  address public lpToken1;

  // Third party contracts
  uint256 public poolId; // Pool id to deposit funds in masterchef
  address public masterchef;

  // Routes
  address[] public outputToLp0Route;
  address[] public outputToLp1Route;
  address[] public outputToWrappedRoute;

  /// @dev Event that is fired each time someone harvests the strat.
  event StratHarvest(address indexed harvester);
  /// @dev Event that is fired each time someone deposit funds the strat.
  event Deposit(uint256 tvl);
  /// @dev Event that is fired each time someone withdraw funds the strat.
  event Withdraw(uint256 tvl);

  /**
   * @dev Configure and start the strategy
   * @param _want LP token we want. example: Joe LP Token (JLP)
   * @param _vault Address of the vault that controls the funds strategy.
   * @param _poolId Pool id to deposit funds in masterchef
   * @param _keeper address to use as alternative owner.
   * @param _unirouter router to use for swaps
   * @param _strategist address where strategist fees go.
   * @param _masterchef address of the MasterChef contract
   * @param _treasuryFeeRecipient Address where to send treasury fees.
   * @param _outputToLp0Route An array of token addresses. must be >= 2.
   * @param _outputToLp1Route An array of token addresses. must be >= 2.
   * @param _outputToWrappedRoute An array of token addresses. must be >= 2.
   **/
  function initialize(
    address _want,
    address _vault,
    uint256 _poolId,
    address _keeper,
    address _unirouter,
    address _strategist,
    address _masterchef,
    address _treasuryFeeRecipient,
    address[] memory _outputToLp0Route,
    address[] memory _outputToLp1Route,
    address[] memory _outputToWrappedRoute
  ) external initializer {
    __StratManager_init(_keeper, _strategist, _unirouter, _vault, _treasuryFeeRecipient);
    __UUPSUpgradeable_init();

    want = _want;
    poolId = _poolId;
    masterchef = _masterchef;

    output = _outputToWrappedRoute[0];
    wrapped = _outputToWrappedRoute[_outputToWrappedRoute.length - 1];
    outputToWrappedRoute = _outputToWrappedRoute;

    // setup lp routing
    lpToken0 = IUniswapV2Pair(want).token0();
    require(_outputToLp0Route[0] == output, "outputToLp0Route[0] != output");
    require(_outputToLp0Route[_outputToLp0Route.length - 1] == lpToken0, "outputToLp0Route[last] != lpToken0");
    outputToLp0Route = _outputToLp0Route;

    lpToken1 = IUniswapV2Pair(want).token1();
    require(_outputToLp1Route[0] == output, "outputToLp1Route[0] != output");
    require(_outputToLp1Route[_outputToLp1Route.length - 1] == lpToken1, "outputToLp1Route[last] != lpToken1");
    outputToLp1Route = _outputToLp1Route;

    _giveAllowances();
  }

  /// @dev puts the funds to work
  function deposit() public whenNotPaused {
    uint256 wantBal = IERC20(want).balanceOf(address(this));

    if(wantBal > 0) {
      IMasterChef(masterchef).deposit(poolId, wantBal);
    }
    emit Deposit(balanceOf());
  }

  /// @dev Withdraw the amount `_amount` from the funds deposited
  function withdraw(uint256 _amount) external {
    require(msg.sender == vault, "!vault");

    uint256 wantBal = IERC20(want).balanceOf(address(this));

    if(wantBal < _amount) {
      IMasterChef(masterchef).withdraw(poolId, _amount.sub(wantBal));
      wantBal = IERC20(want).balanceOf(address(this));
    }

    if(wantBal > _amount) {
      wantBal = _amount;
    }

    if(tx.origin != owner() && !paused()) {
      uint256 withdrawalFeeAmount = wantBal.mul(withdrawalFee).div(PERCENTAGE_DENOMINATOR);
      wantBal = wantBal.sub(withdrawalFeeAmount);
    }

    IERC20(want).safeTransfer(vault, wantBal);

    emit Withdraw(balanceOf());
  }

  /// @dev compounds earnings and charges performance fee
  function harvest() external whenNotPaused {
    IMasterChef(masterchef).deposit(poolId, 0);
    chargeFees();
    addLiquidity();
    deposit();

    emit StratHarvest(msg.sender);
  }

  /// @dev performance fees
	function chargeFees() internal {
		uint256 toWrapped = IERC20(output)
      .balanceOf(address(this)).mul(PERFORMANCE_FEE).div(PERCENTAGE_DENOMINATOR);

    IUniswapV2Router02(unirouter).swapExactTokensForTokens(
      toWrapped, 0, outputToWrappedRoute, address(this), block.timestamp
    );

		uint256 wrappedBal = IERC20(wrapped).balanceOf(address(this));

		uint256 govFeeAmount = wrappedBal.mul(GOVERNANCE_POOL_FEE).div(PERCENTAGE_DENOMINATOR);
		IERC20(wrapped).safeTransfer(owner(), govFeeAmount);

    uint256 treasuryFeeAmount = wrappedBal.mul(TREASURY_FEE).div(PERCENTAGE_DENOMINATOR);
		IERC20(wrapped).safeTransfer(treasuryFeeRecipient, treasuryFeeAmount);

		uint256 strategistFee = wrappedBal.mul(STRATEGIST_FEE).div(PERCENTAGE_DENOMINATOR);
		IERC20(wrapped).safeTransfer(strategist, strategistFee);

		uint256 callFeeAmount = wrappedBal.mul(HARVEST_CALL_FEE).div(PERCENTAGE_DENOMINATOR);
		IERC20(wrapped).safeTransfer(tx.origin, callFeeAmount);

    // TODO: Define to which address BURNED_FEE is sent
	}

  /// @dev Adds liquidity to AMM and gets more LP tokens.
	function addLiquidity() internal {
		uint256 outputHalf = IERC20(output).balanceOf(address(this)).div(2);

		if(lpToken0 != output) {
			IUniswapV2Router02(unirouter).swapExactTokensForTokens(
				outputHalf,
				0,
				outputToLp0Route,
				address(this),
				block.timestamp
			);
		}

		if(lpToken1 != output) {
			IUniswapV2Router02(unirouter).swapExactTokensForTokens(
				outputHalf,
				0,
				outputToLp1Route,
				address(this),
				block.timestamp
			);
		}

		uint256 lp0Bal = IERC20(lpToken0).balanceOf(address(this));
		uint256 lp1Bal = IERC20(lpToken1).balanceOf(address(this));
		IUniswapV2Router02(unirouter).addLiquidity(
      lpToken0, lpToken1, lp0Bal, lp1Bal, 1, 1, address(this), block.timestamp
    );
	}

  /// @dev calculate the total underlaying 'want' held by the strat.
	function balanceOf() public view returns (uint256) {
		return balanceOfWant().add(balanceOfPool());
	}

	/// @dev it calculates how much 'want' this contract holds.
	function balanceOfWant() public view returns (uint256) {
		return IERC20(want).balanceOf(address(this));
	}

	/// @dev it calculates how much 'want' the strategy has working in the farm.
	function balanceOfPool() public view returns (uint256) {
		(uint256 _amount, ) = IMasterChef(masterchef).userInfo(poolId, address(this));
		return _amount;
	}

  // called as part of strat migration. Sends all the available funds back to the vault.
	function retireStrat() external {
		require(msg.sender == vault, "!vault");

		IMasterChef(masterchef).emergencyWithdraw(poolId);

		uint256 wantBal = IERC20(want).balanceOf(address(this));
		IERC20(want).transfer(vault, wantBal);
	}

  // pauses deposits and withdraws all funds from third party systems.
	function panic() public onlyManager {
		pause();
		IMasterChef(masterchef).emergencyWithdraw(poolId);
	}

  function pause() public onlyManager {
		_pause();

		_removeAllowances();
	}

	function unpause() external onlyManager {
		_unpause();

		_giveAllowances();

		deposit();
	}

  function _giveAllowances() internal {
    IERC20(want).safeApprove(masterchef, type(uint256).max);
    IERC20(output).safeApprove(unirouter, type(uint256).max);

    IERC20(lpToken0).safeApprove(unirouter, 0);
    IERC20(lpToken0).safeApprove(unirouter, type(uint256).max);

    IERC20(lpToken1).safeApprove(unirouter, 0);
    IERC20(lpToken1).safeApprove(unirouter, type(uint256).max);
  }

  function _removeAllowances() internal {
		IERC20(want).safeApprove(masterchef, 0);
		IERC20(output).safeApprove(unirouter, 0);
		IERC20(lpToken0).safeApprove(unirouter, 0);
		IERC20(lpToken1).safeApprove(unirouter, 0);
	}

  function _authorizeUpgrade(address) internal override onlyOwner {}
}
