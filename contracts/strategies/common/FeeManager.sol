/// Copyright (c) 2022 Stablecomp ltd.
/// opensource@stablecomp.com
/// SPDX-License-Identifier: MIT
/// Licensed under the MIT License;
/// You may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
/// https://github.com/stablecomp/contracts/blob/main/LICENSE
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.

pragma solidity 0.8.8;

import "./StratManager.sol";

abstract contract FeeManager is StratManager {
  uint256 public constant PERCENTAGE_DENOMINATOR = 10000; // 100%
  // Performance fee: 6% of returns distributed as follows
  uint256 public constant PERFORMANCE_FEE = 600; // 6%

  // 3% to the Governance Pool => 50% of Performance fee
  uint256 public constant GOVERNANCE_POOL_FEE = 5000; // 50%
  // 1% to the treasury => 16.7% of Performance fee
  uint256 public constant TREASURY_FEE = 1670; // 16.7%
  // 0,5% to the vault strategist => 8.3% of Performance fee
  uint256 public constant STRATEGIST_FEE = 830; // 8.3%
  // 0,5% to the harvest function => 8.3% of Performance fee
  uint256 public HARVEST_CALL_FEE = 830; // 8.3%
  // 1% burned => 16.7% of Performance fee
  uint256 public constant BURNED_FEE = 1670; // 16.7%

  uint256 public constant WITHDRAWAL_FEE_CAP = 500; // 5%
  uint256 public withdrawalFee = 10; // 0.1%

  function setWithdrawalFee(uint256 _fee) public onlyManager {
    require(_fee <= WITHDRAWAL_FEE_CAP, "!cap");

    withdrawalFee = _fee;
  }
}
