/// Copyright (c) 2022 Stablecomp ltd.
/// opensource@stablecomp.com
/// SPDX-License-Identifier: MIT
/// Licensed under the MIT License;
/// You may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
/// https://github.com/stablecomp/contracts/blob/main/LICENSE
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.

pragma solidity 0.8.8;

import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20BurnableUpgradeable.sol";

/**
 * @title SCOMP Token
 * @dev SCOMP is the native governance token of Stablecomp.
**/
contract SCOMP is ERC20Upgradeable, AccessControlUpgradeable, ERC20BurnableUpgradeable, UUPSUpgradeable {
	using SafeMathUpgradeable for uint256;
	using SafeERC20Upgradeable for IERC20Upgradeable;

	/**
	 * @param name name of ERC20 token
	 * @param symbol symbol of ERC20 token
	**/
	function initialize(string memory name, string memory symbol) external initializer {
		__ERC20_init(name, symbol);
		__AccessControl_init();
		__ERC20Burnable_init();
		__UUPSUpgradeable_init();

		// TODO: Implement the different roles to be used in the Token
		_grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
		_mint(msg.sender, uint256(200_000_000).mul(uint256(10)**decimals()));
	}

	// TODO: Change authorized role when defined
	function _authorizeUpgrade(address) internal override onlyRole(DEFAULT_ADMIN_ROLE) {}
}
