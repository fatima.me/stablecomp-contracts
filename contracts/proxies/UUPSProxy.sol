/// Copyright (c) 2022 Stablecomp ltd.
/// opensource@stablecomp.com
/// SPDX-License-Identifier: MIT
/// Licensed under the MIT License;
/// You may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
/// https://github.com/stablecomp/contracts/blob/main/LICENSE
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.

pragma solidity 0.8.8;

import "@openzeppelin/contracts/proxy/ERC1967/ERC1967Proxy.sol";

/**
 * @dev this proxy is just a wrapper to make ERC1967Proxy compatible with hardhat-deploy
 */
contract UUPSProxy is ERC1967Proxy {
	constructor(
		address _logic,
		address,
		bytes memory _data
	) ERC1967Proxy(_logic, _data) {} // solhint-disable-line
}