/// Copyright (c) 2022 Stablecomp ltd.
/// opensource@stablecomp.com
/// SPDX-License-Identifier: MIT
/// Licensed under the MIT License;
/// You may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
/// https://github.com/stablecomp/contracts/blob/main/LICENSE
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.

pragma solidity 0.8.8;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@uniswap/lib/contracts/libraries/Babylonian.sol";
import "@uniswap/v2-periphery/contracts/interfaces/IWETH.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@uniswap/v3-core/contracts/libraries/LowGasSafeMath.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";

import "./interfaces/IStablecompVaultV1.sol";

/**
 * @title The one-click function for auto composition
 * @dev OneClickUniV2 Allows users to deposit the most popular stablecoins, USDC, BUSD,
 * and USDT, as single tokens and benefit from one-click auto compounding
 **/
contract OneClickUniV2 is OwnableUpgradeable, UUPSUpgradeable {
	using SafeERC20 for IERC20;
	using LowGasSafeMath for uint256;
	using SafeERC20 for IStablecompVaultV1;

	address public wrapped; // Wrapped Native Token. Example: WFTM => Fantom Token (FTM)
	IUniswapV2Router02 public router;
	uint256 public constant minimumAmount = 1000;

	function initialize(address _router, address _wrapped) external initializer {
		__Ownable_init();
		__UUPSUpgradeable_init();

		wrapped = _wrapped;
		router = IUniswapV2Router02(_router);
	}

	receive() external payable {
		assert(msg.sender == wrapped);
	}

	/**
	 * @dev Starting point for automatic composition with the network's native token.
	 * @param vault Address of the vault that controls the funds strategy.
	 * @param tokenAmountOutMin The minimum amount of output tokens that must be received for the
	 * transaction not to revert.
	 **/
	function scompInTokenNative(address vault, uint256 tokenAmountOutMin) external payable {
		require(msg.value >= minimumAmount, "Input amount below minimum");

		// assign the tokens to this contract
		IWETH(wrapped).deposit{ value: msg.value }();

		_swapAndStake(wrapped, vault, tokenAmountOutMin);
	}

	/**
	 * @dev Starting point for auto composition.
	 * @param vault Address of the vault that controls the funds of the strategy
	 * @param tokenIn Address of the token to deposit.
	 * @param tokenInAmount Amount of tokens to deposit
	 * @param tokenAmountOutMin The minimum amount of output tokens that must be received for the
	 * transaction not to revert.
	 **/
	function scompIn(
		address vault,
		address tokenIn,
		uint256 tokenInAmount,
		uint256 tokenAmountOutMin
	) external {
		require(tokenInAmount >= minimumAmount, "Input amount below minimum");

		require(
			IERC20(tokenIn).allowance(msg.sender, address(this)) >= tokenInAmount,
			"Input token is not approved"
		);

		// assign the tokens to this contract
		IERC20(tokenIn).safeTransferFrom(msg.sender, address(this), tokenInAmount);

		_swapAndStake(tokenIn, vault, tokenAmountOutMin);
	}

	/**
	 * @dev Withdraw `withdrawAmount` amount of funds deposited in `vault`
	 * @param _vault Address of the vault that controls the funds of the strategy
	 * @param withdrawAmount Amount you want to withdraw
	 **/
	function scompOut(address _vault, uint256 withdrawAmount) external {
		(IUniswapV2Pair pair, IStablecompVaultV1 vault) = _getVaultPair(_vault);

		// transfer vault ownership tokens to this contract to withdraw funds and accumulate rewards
		vault.safeTransferFrom(msg.sender, address(this), withdrawAmount);
		vault.withdraw(withdrawAmount);

		// If the tokens of the pair are not the native token of the network, the liquidity is
		// removed and sent directly to the user
		if(pair.token0() != wrapped && pair.token1() != wrapped) {
			return _removeLiquidity(address(pair), msg.sender);
		}

		// remove liquidity and send the underlying assets to this contract to withdraw the balance
		// sent to the network's native token wrapper
		_removeLiquidity(address(pair), address(this));

		address[] memory tokens = new address[](2);
		tokens[0] = pair.token0();
		tokens[1] = pair.token1();

		_returnAssets(tokens);
	}

	/**
	 * @dev Withdraw the amount `withdrawAmount` from the funds deposited in `vault`. A swap
	 * is made for `desiredToken`
	 * @param _vault Address of the vault that controls the funds of the strategy
	 * @param desiredToken Address of the token to be withdrawn
	 * @param withdrawAmount Amount you want to withdraw
	 * @param desiredTokenOutMin The minimum amount of output tokens that must be received for the
	 * transaction not to revert.
	 **/
	function scompOutAndSwap(
		address _vault,
		address desiredToken,
		uint256 withdrawAmount,
		uint256 desiredTokenOutMin
	) external {
		(IUniswapV2Pair pair, IStablecompVaultV1 vault) = _getVaultPair(_vault);
		address token0 = pair.token0(); // gas savings
		address token1 = pair.token1(); // gas savings
		require(
			token0 == desiredToken || token1 == desiredToken,
			"Desired token not present in liquidity pair"
		);

		// transfer vault ownership tokens to this contract to withdraw funds and accumulated rewards
		vault.safeTransferFrom(msg.sender, address(this), withdrawAmount);
		vault.withdraw(withdrawAmount);

		// remove liquidity and send the underlying assets to this contract to withdraw the balance
		// sent to the network's native token wrapper
		_removeLiquidity(address(pair), address(this));

		address swapToken = token1 == desiredToken ? token0 : token1;
		address[] memory path = new address[](2);
		path[0] = swapToken;
		path[1] = desiredToken;

		_approveTokenIfNeeded(path[0], address(router));
		router.swapExactTokensForTokens(
			IERC20(swapToken).balanceOf(address(this)),
			desiredTokenOutMin,
			path,
			address(this),
			block.timestamp
		);

		_returnAssets(path);
	}

	/**
	 * Calculates the amount of the input token (tokenIn) that must be swap for tokenB
	 * to add liquidity to the pool
	 * @param vault Address of the vault that controls the funds of the strategy
	 * @param tokenIn input token address
	 * @param fullInvestmentIn Amount to deposit of the input token
	 * ------------------------------
	 * @return swapAmountIn Amount of tokenIn to swap per tokenB
	 * @return swapAmountOut maximum output amount of the tokenB
	 * @return swapTokenOut output token address 'tokenB'
	**/
	function estimateSwap(
		address vault,
		address tokenIn,
		uint256 fullInvestmentIn
	) external view returns (
		uint256 swapAmountIn,
		uint256 swapAmountOut,
		address swapTokenOut
	) {
		checkWETH();
		(IUniswapV2Pair pair,) = _getVaultPair(vault);

		bool isInputA = pair.token0() == tokenIn;
		require(isInputA || pair.token1() == tokenIn, "Input token not present in liquidity pair");

		// Get the reserves of token0 and token1 used to price trades and distribute liquidity
		(uint256 reserveA, uint256 reserveB, ) = pair.getReserves();
		// Sort reservations to match the input token `tokenIn`
		(reserveA, reserveB) = isInputA ? (reserveA, reserveB) : (reserveB, reserveA);

		swapAmountIn = _getSwapAmount(fullInvestmentIn, reserveA, reserveB);
		swapAmountOut = router.getAmountOut(swapAmountIn, reserveA, reserveB);
		// Determine output token
		swapTokenOut = isInputA ? pair.token1() : pair.token0();
	}

	function _authorizeUpgrade(address) internal override onlyOwner {}

	function checkWETH() private view returns (bool isValid) {
		isValid = wrapped == router.WETH();
		require(isValid, "wrapped address not matching Router.WETH()");
	}

	/**
	 * @dev Liquidity provider withdraws funds (and the accumulated rewards) and his pool
	 * ownership tokens are burned (destroyed).
	 * @param pair Address of the pair
	 * @param to Recipient of the underlying assets.
	**/
	function _removeLiquidity(address pair, address to) private {
		IERC20(pair).safeTransfer(pair, IERC20(pair).balanceOf(address(this)));
		(uint256 amount0, uint256 amount1) = IUniswapV2Pair(pair).burn(to);

		require(amount0 >= minimumAmount, "UniswapV2Router: INSUFFICIENT_A_AMOUNT");
		require(amount1 >= minimumAmount, "UniswapV2Router: INSUFFICIENT_B_AMOUNT");
	}

	function _getVaultPair(address _vault) private view returns (
		IUniswapV2Pair pair,
		IStablecompVaultV1 vault
	) {
		vault = IStablecompVaultV1(_vault);
		pair = IUniswapV2Pair(vault.want());
		require(pair.factory() == router.factory(), "Incompatible liquidity pair factory");
	}

	/**
	 * @dev Allows to create tokens of liquidity pools and make deposits in the vault
	 * @param tokenIn input token address
	 * @param _vault Address of the vault that controls the funds of the strategy
	 * @param tokenAmountOutMin The minimum amount of output tokens that must be received for the
	 * transaction not to revert.
	**/
	function _swapAndStake(
		address tokenIn,
		address _vault,
		uint256 tokenAmountOutMin
	) private {
		(IUniswapV2Pair pair, IStablecompVaultV1 vault) = _getVaultPair(_vault);

		(uint256 reserveA, uint256 reserveB, ) = pair.getReserves();
		require(reserveA > minimumAmount && reserveB > minimumAmount, "Liquidity pair reserves too low");

		bool isInputA = pair.token0() == tokenIn;
		require(isInputA || pair.token1() == tokenIn, "Input token not present in liquidity pair");

		address[] memory path = new address[](2);
		path[0] = tokenIn;
		path[1] = isInputA ? pair.token1() : pair.token0();

		uint256 fullInvestment = IERC20(tokenIn).balanceOf(address(this));
		uint256 swapAmountIn;
		if(isInputA) {
			swapAmountIn = _getSwapAmount(fullInvestment, reserveA, reserveB);
		} else {
			swapAmountIn = _getSwapAmount(fullInvestment, reserveB, reserveA);
		}

		_approveTokenIfNeeded(path[0], address(router));
		// swapedAmounts: The input token amount and all subsequent output token amounts.
		uint256[] memory swapedAmounts = router.swapExactTokensForTokens(
			swapAmountIn,
			tokenAmountOutMin,
			path,
			address(this),
			block.timestamp
		);

		_approveTokenIfNeeded(path[1], address(router));
		(, , uint256 amountLiquidity) = router.addLiquidity(
			path[0],
			path[1],
			fullInvestment.sub(swapedAmounts[0]),
			swapedAmounts[1],
			1,
			1,
			address(this),
			block.timestamp
		);

		_approveTokenIfNeeded(address(pair), address(vault));
		vault.deposit(amountLiquidity);

		vault.safeTransfer(msg.sender, vault.balanceOf(address(this)));
		_returnAssets(path);
	}

	function _returnAssets(address[] memory tokens) private {
		uint256 balance;
		for (uint256 i; i < tokens.length; i++) {
			balance = IERC20(tokens[i]).balanceOf(address(this));
			if(balance > 0) {
				if(tokens[i] == wrapped) {
					IWETH(wrapped).withdraw(balance);
					(bool success, ) = msg.sender.call{ value: balance }(new bytes(0));
					require(success, "ETH transfer failed");
				} else {
					IERC20(tokens[i]).safeTransfer(msg.sender, balance);
				}
			}
		}
	}

	/**
	 * @dev Calculates the amount of the input token (tokenIn) that must be swap for tokenB
	 * to add liquidity to the pool
	 * @param investmentA full investment of token A (tokenIn)
	 * @param reserveA token A reserves
	 * @param reserveB token B reserves
	 * @return swapAmount Amount of tokens to be swap
	**/
	function _getSwapAmount(
		uint256 investmentA,
		uint256 reserveA,
		uint256 reserveB
	) private view returns (uint256 swapAmount) {
		uint256 halfInvestment = investmentA / 2;
		uint256 nominator = router.getAmountOut(halfInvestment, reserveA, reserveB);
		uint256 denominator = router.quote(
			halfInvestment, reserveA.add(halfInvestment), reserveB.sub(nominator)
		);

		swapAmount = investmentA.sub(
			Babylonian.sqrt((halfInvestment * halfInvestment * nominator) / denominator)
		);
	}

	function _approveTokenIfNeeded(address token, address spender) private {
		if(IERC20(token).allowance(address(this), spender) == 0) {
			IERC20(token).safeApprove(spender, type(uint256).max);
		}
	}
}
