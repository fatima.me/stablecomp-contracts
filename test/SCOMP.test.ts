import { setupUser } from '../helpers/ethers';
import { Accounts } from '../typescript/hardhat';
import { assert, expect } from './utils/chaiSetup';
import { SCOMP, TokenVesting } from '../build/types';
import { formatEther, parseEther } from 'ethers/lib/utils';
import { deployments, ethers, getNamedAccounts } from 'hardhat';

async function setup() {
  await deployments.fixture(['SCOMP', 'TokenVesting']);

  const contracts = {
    SCOMP: (await ethers.getContract('SCOMP')) as SCOMP,
    TokenVesting: (await ethers.getContract('TokenVesting')) as TokenVesting,
  }

  const accounts = (await getNamedAccounts()) as Accounts;

  return {
    accounts,
    contracts,
  }
}

/* constants */
const MINUTE = 60 * 60;
const HOUR = MINUTE * 60;
const DAY = HOUR * 24;

describe('SCOMP Contract', () => {
  it('should be deployed', async () => {
    const { contracts } = await setup();

    assert.ok(contracts.SCOMP.address);
  })

  describe('Token vesting contract', () => {
    it('Should be deployed', async () => {
      const { contracts } = await setup();
      assert.ok(contracts.TokenVesting.address);
    });

    it('Vested amount must be greater than zero', async () => {
      const { contracts: { TokenVesting }, accounts } = await setup();

      await expect(TokenVesting.deposit(0, 0, accounts.user1))
      .to.be.revertedWith('Vested amount must be greater than zero');
    });

    it('Beneficiary cannot be zero address', async () => {
      const { contracts: { TokenVesting } } = await setup();

      const releaseTime = +new Date() + DAY;
      const beneficiary = ethers.constants.AddressZero;
      await expect(TokenVesting.deposit(parseEther('1'), releaseTime, beneficiary))
      .to.be.revertedWith('Beneficiary cannot be zero address');
    });

    it('Successful deposit', async () => {
      const { accounts, contracts } = await setup();
      const { SCOMP, TokenVesting } = contracts;

      const amount = parseEther('10');
      const beneficiary = accounts.user1;
      const releaseTime = +new Date() + DAY;
      const tokenVestingBalance = await SCOMP.balanceOf(TokenVesting.address);

      const spender = TokenVesting.address;

      await expect(TokenVesting.deposit(amount, releaseTime, beneficiary))
      .to.revertedWith('ERC20: insufficient allowance');

      await SCOMP.increaseAllowance(spender, amount);

      const lockBoxId = (await TokenVesting.getLockBoxCount()).toNumber();

      await expect(TokenVesting.deposit(amount, releaseTime, beneficiary))
      .to.emit(TokenVesting, 'LogLockBoxDeposit')
      .withArgs(lockBoxId, amount, releaseTime, beneficiary);

      expect(await SCOMP.balanceOf(TokenVesting.address))
      .to.be.equal(tokenVestingBalance.add(amount))
    });

    it('Successful withdraw', async () => {
      const {contracts: {SCOMP, TokenVesting}, accounts: {deployer, user1}} = await setup();

      const amount = parseEther('10');
      const releaseTime = +new Date() + DAY;

      await SCOMP.increaseAllowance(TokenVesting.address, amount.mul(2));
      await TokenVesting.deposit(amount, releaseTime, user1); // ID: 0
      await TokenVesting.deposit(amount, 0, user1); // ID: 1

      const tokenVestingBalance = await SCOMP.balanceOf(TokenVesting.address);

      await expect(TokenVesting.withdraw(0))
      .to.be.revertedWith('Unauthorized address');

      const user1ConnectContract = await setupUser(user1, { TokenVesting });

      // Should fail if tokens are withdrawn while they are still locked
      await expect(user1ConnectContract.TokenVesting.withdraw(0))
      .to.be.revertedWith('Tokens are still locked');

      const lockBoxId = 1;

      await expect(user1ConnectContract.TokenVesting.withdraw(lockBoxId))
      .to.emit(TokenVesting, 'LogLockBoxWithdrawal')
      .withArgs(lockBoxId, user1, amount);

      expect(await SCOMP.balanceOf(TokenVesting.address))
      .to.be.equal(tokenVestingBalance.sub(amount))

    });
  });
})