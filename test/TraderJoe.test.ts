import { parseEther, formatEther } from 'ethers/lib/utils';
import { Accounts } from '../typescript/hardhat';
import { assert, expect } from './utils/chaiSetup';
import { deployments, ethers, getNamedAccounts } from 'hardhat';
import {
  JoePair as IJoePair,
  ERC20Mock as IERC20Mock,
  StrategyDualLP as IStrategyDualLP,
  JoeFactory as IJoeFactory,
  JoeRouter02Mock as IJoeRouter02Mock,
  StablecompVaultV1 as IStablecompVaultV1,
  OneClickTraderJoeV1 as IOneClickTraderJoeV1,
} from '../build/types';
import { setupUser } from '../helpers/ethers';

async function setup() {
  await deployments.fixture([
    'USDC',
    'USDC.e',
    'JoeFactory',
    'JoeRouter02Mock',
    'OneClickTraderJoeV1',
    'VaultTraderJoe_USDC_USDC.e',
    'StrategyDualLPTraderJoe_USDC_USDC.e',
  ]);

  const [
    USDC,
    USDCe,
    JoeFactory,
    JoeRouter02,
    StrategyDualLP,
    StablecompVaultV1,
    OneClickTraderJoeV1,
  ] = await Promise.all([
    ethers.getContract<IERC20Mock>('USDC'),
    ethers.getContract<IERC20Mock>('USDC.e'),
    ethers.getContract<IJoeFactory>('JoeFactory'),
    ethers.getContract<IJoeRouter02Mock>('JoeRouter02Mock'),
    ethers.getContract<IStrategyDualLP>('StrategyDualLPTraderJoe_USDC_USDC.e'),
    ethers.getContract<IStablecompVaultV1>('VaultTraderJoe_USDC_USDC.e'),
    ethers.getContract<IOneClickTraderJoeV1>('OneClickTraderJoeV1'),
  ])

  const accounts = (await getNamedAccounts()) as Accounts;

  return {
    accounts,
    contracts: {
      USDC,
      USDCe,
      JoeFactory,
      JoeRouter02,
      StrategyDualLP,
      StablecompVaultV1,
      OneClickTraderJoeV1,
    },
  }
}

describe('Trader Joe AMM', () => {
  it('Contract OneClickTraderJoeV1 must be deployed', async () => {
    const { contracts } = await setup();
    assert.ok(contracts.OneClickTraderJoeV1.address);
  })

  it('Contract StablecompVaultV1 must be deployed', async () => {
    const { contracts } = await setup();
    assert.ok(contracts.StablecompVaultV1.address);
  })

  it('Contract StrategyDualLP must be deployed', async () => {
    const { contracts } = await setup();
    assert.ok(contracts.StrategyDualLP.address);
  })

  it('Deposit and withdraw funds with one click function', async () => {
    const { contracts, accounts } = await setup();
    const {
      USDC,
      USDCe,
      JoeRouter02,
      StablecompVaultV1,
      OneClickTraderJoeV1,
    } = contracts;
    const { user1, deployer } = accounts;

    // Send balances of the owner of the contracts to user1 for a clearer vision throughout the
    // process of entering and exiting the system
    await USDC.transfer(user1, parseEther('100'));

    const balanceUSDC = await USDC.balanceOf(user1);
    // Balance should be greater than 0
    expect(0).to.be.below(balanceUSDC);

    /* Deposit balance in contract OneClickTraderJoeV1 to start with the auto-composition */

    // connect user1 to contracts
    const user1Connect = await setupUser(user1, contracts);

    // Token to deposit for auto composition
    const tokenInAmount = parseEther('50');

    // It should be fail because amount deposited is below of minimum
    await expect(
      user1Connect.OneClickTraderJoeV1.scompIn(
        StablecompVaultV1.address,
        USDC.address,
        1,
        1,
      )
    ).to.be.revertedWith('Input amount below minimum');

    // It should fail because the OneClickUniV2 contract has not been approved so you can use
    // out tokens
    await expect(
      user1Connect.OneClickTraderJoeV1.scompIn(
        StablecompVaultV1.address,
        USDC.address,
        tokenInAmount,
        1,
      )
    ).to.be.revertedWith('Input token is not approved');

    await user1Connect.USDC.approve(OneClickTraderJoeV1.address, tokenInAmount);

    await expect(
      user1Connect.OneClickTraderJoeV1.scompIn(
        ethers.constants.AddressZero,
        USDC.address,
        tokenInAmount,
        1,
      )
    ).to.be.revertedWith('function call to a non-contract account');

    // Should fail because have not liquidity in the pool
    await expect(
      user1Connect.OneClickTraderJoeV1.scompIn(
        StablecompVaultV1.address,
        USDC.address,
        tokenInAmount,
        1,
      )
    ).to.be.revertedWith('Liquidity pair reserves too low');

    /* Add liquidity to test the entire process */
    const amountTokenADesired = parseEther('900');
    const amountTokenBDesired = parseEther('900');
    const deadlineAddLiquidity = +new Date();
    await USDC.approve(JoeRouter02.address, amountTokenADesired);
    await USDCe.approve(JoeRouter02.address, amountTokenBDesired);
    await JoeRouter02.addLiquidity(
      USDC.address,
      USDCe.address,
      amountTokenADesired,
      amountTokenBDesired,
      1,
      1,
      deployer,
      deadlineAddLiquidity,
    )

    // Balance of ownership tokens before using the OneClickUniV2 contract
    const balanceOwnershipTokenBefore = await StablecompVaultV1.balanceOf(user1);
    // Balance of tokens held by the system (LP tokens, and other reward tokens that are part
    // of the strategy)
    const balanceVaultBefore = await StablecompVaultV1.balance();

    // calculate the ideal value for tokenAmountOutMin
    const estimateSwap = await user1Connect.OneClickTraderJoeV1.estimateSwap(
      StablecompVaultV1.address,
      USDC.address,
      tokenInAmount
    );
    const tokenAmountOutMin = estimateSwap.swapAmountOut;

    await user1Connect.OneClickTraderJoeV1.scompIn(
      StablecompVaultV1.address,
      USDC.address,
      tokenInAmount,
      tokenAmountOutMin
    );

    // Balance of ownership tokens after using the OneClickUniV2 contract
    const balanceOwnershipTokenAfter = await StablecompVaultV1.balanceOf(user1);
    // Balance of tokens held by the system (LP tokens, and other reward tokens that are part
    // of the strategy)
    const balanceVaultAfter = await StablecompVaultV1.balance();

    expect(balanceOwnershipTokenBefore).to.be.below(balanceOwnershipTokenAfter);
    expect(balanceVaultBefore).to.be.below(balanceVaultAfter);

    // TODO: do scompOut() tests
  })
})