import { parseEther } from 'ethers/lib/utils';
import { TOKENS } from '../addressbook/tokens';
import { assert, expect } from './utils/chaiSetup';
import { CURVE_CONTRACTS } from '../addressbook/amm/curve';
import { UNISWAP_CONTRACTS } from '../addressbook/amm/uniswap';
import { ethers, getNamedAccounts, deployments } from 'hardhat';

/* types */
import type { Accounts } from '../typescript/hardhat';
import type {
  WETH9 as IWETH9,
  ERC20Mock as IERC20Mock,
  OneClickCurveLPV1 as IOneClickCurveLPV1,
  StablecompVaultV1 as IStablecompVaultV1,
  UniswapV2Router02 as IUniswapV2Router02,
  StrategyCurve3Pool as IStrategyCurve3Pool,
  StableSwap3PoolMock as IStableSwap3PoolMock,
} from '../build/types';

async function setup() {
  await deployments.fixture([
    'VaultCurve3pool',
    'OneClickCurveLPV1',
    'StrategyCurve3Pool',
  ])

  const accounts = (await getNamedAccounts()) as Accounts;

  const { _3pool } = CURVE_CONTRACTS.mainnetETH;

  const UNISWAP = UNISWAP_CONTRACTS.mainnetETH.V2;
  const {
    USDT: USDT_ADDRESS,
    WETH: WETH_ADDRESS,
    USDC: USDC_ADDRESS,
    DAI: DAI_ADDRESS,
  } = TOKENS.mainnetETH;

  const [
    WETH,
    UniswapV2Router02,
    USDT,
    StableSwap3PoolMock,
    USDC,
    DAI,
    OneClickCurveLPV1,
    StrategyCurve3Pool,
    VaultCurve3pool,
  ] = await Promise.all([
    ethers.getContractAt<IWETH9>('WETH9', WETH_ADDRESS),
    ethers.getContractAt<IUniswapV2Router02>('UniswapV2Router02', UNISWAP.Router02),
    ethers.getContractAt<IERC20Mock>('ERC20Mock', USDT_ADDRESS),
    ethers.getContractAt<IStableSwap3PoolMock>('StableSwap3PoolMock', _3pool.pool),
    ethers.getContractAt<IERC20Mock>('ERC20Mock', USDC_ADDRESS),
    ethers.getContractAt<IERC20Mock>('ERC20Mock', DAI_ADDRESS),
    ethers.getContract<IOneClickCurveLPV1>('OneClickCurveLPV1'),
    ethers.getContract<IStrategyCurve3Pool>('StrategyCurve3Pool'),
    ethers.getContract<IStablecompVaultV1>('VaultCurve3pool'),
  ])

  return {
    accounts,
    contracts: {
      DAI,
      WETH,
      USDT,
      USDC,
      VaultCurve3pool,
      OneClickCurveLPV1,
      UniswapV2Router02,
      StrategyCurve3Pool,
      StableSwap3PoolMock,
    }
  }
}

interface getBalanceParams {
  wrapped: IWETH9;
  spender: string;
  token: IERC20Mock;
  amountInETH: string;
  swap: IUniswapV2Router02;
}

/**
 * To be able to complete the tests it is necessary to obtain some existing tokens in the
 * pool. We use Uniswap as the preferred AMM to obtain the tokens
**/
async function getBalance(params: getBalanceParams) {
  const {
    swap,
    token,
    wrapped,
    spender,
    amountInETH
  } = params;

  const amountOutMin = 0;
  const deadline = +new Date();
  const amount = parseEther(amountInETH);

  await wrapped.deposit({ value: amount });
  await wrapped.approve(swap.address, amount);

  await swap.swapExactTokensForTokens(
    amount,
    amountOutMin,
    [ wrapped.address, token.address ],
    spender,
    deadline
  );
}

describe('Curve protocole', () => {
  describe('Contract OneClickV1', () => {
    it('Must be deployed', async () => {
      const { contracts } = await setup();
      assert.ok(contracts.OneClickCurveLPV1.address);
    });

    describe('scompIn function', () => {
      it('Should fail if the strategy does not belong to the vault', async () => {
        const { AddressZero } = ethers.constants;
        const { contracts: { VaultCurve3pool, OneClickCurveLPV1 } } = await setup();

        await expect(
          OneClickCurveLPV1.scompIn(VaultCurve3pool.address, AddressZero, [AddressZero], [0])
        ).to.be.revertedWith('Incompatible strategy')
      });

      it('Should fail if the tokensIn.length does not match the size pool', async () => {
        const { contracts: {
          VaultCurve3pool,
          OneClickCurveLPV1,
          StrategyCurve3Pool,
        } } = await setup();

        await expect(
          OneClickCurveLPV1.scompIn(
            VaultCurve3pool.address,
            StrategyCurve3Pool.address,
            [ ethers.constants.AddressZero ],
            [ parseEther('1') ]
          )
        ).to.revertedWith('The data provided does not match the size of the pool')
      });

      it('Should fail if the oneClick contract to use the tokens to deposit is not approved',
        async () => {
          const { contracts, accounts } = await setup();
          const {
            WETH,
            USDT,
            VaultCurve3pool,
            UniswapV2Router02,
            OneClickCurveLPV1,
            StrategyCurve3Pool,
          } = contracts;

          const { deployer } = accounts;

          await getBalance({
            token: USDT,
            wrapped: WETH,
            amountInETH: '1',
            spender: deployer,
            swap: UniswapV2Router02,
          });

          const balanceInUSDT = await USDT.balanceOf(deployer);

          // Balance should be greater than 0
          expect(0).to.be.below(balanceInUSDT);

          const { AddressZero } = ethers.constants;

          // Tokens that should not be deposited should be added to the input token array as
          // address zero and in the amounts array the amount must be passed as zero
          await expect(
            OneClickCurveLPV1.scompIn(
              VaultCurve3pool.address,
              StrategyCurve3Pool.address,
              [ AddressZero, AddressZero, USDT.address ],
              [ 0, 0, balanceInUSDT ]
            )
          ).to.revertedWith('Input token is not approved')
        }
      );

      it('Deposit and withdraw funds from a single token in one click', async () => {
        const { contracts, accounts } = await setup();
        const {
          WETH,
          USDT,
          VaultCurve3pool,
          UniswapV2Router02,
          OneClickCurveLPV1,
          StrategyCurve3Pool,
        } = contracts;

        const { deployer } = accounts;

        // Swap WETH for USDT tokens
        await getBalance({
          token: USDT,
          wrapped: WETH,
          amountInETH: '1',
          spender: deployer,
          swap: UniswapV2Router02,
        });

        const balanceInUSDT = await USDT.balanceOf(deployer);

        // Balance should be greater than 0
        expect(0).to.be.below(balanceInUSDT);

        const { AddressZero } = ethers.constants;

        await USDT.approve(OneClickCurveLPV1.address, balanceInUSDT);

        await OneClickCurveLPV1.scompIn(
          VaultCurve3pool.address,
          StrategyCurve3Pool.address,
          [ AddressZero, AddressZero, USDT.address ],
          [ 0, 0, balanceInUSDT ],
        )
      });
    });
  });
});