import { parseEther, formatEther } from 'ethers/lib/utils';
import { Accounts } from '../typescript/hardhat';
import { assert, expect } from './utils/chaiSetup';
import { deployments, ethers, getNamedAccounts } from 'hardhat';
import {
  ERC20Mock as IERC20Mock,
  WrappedFtm as IWrappedFtm,
  UniswapV2Pair as IUniswapV2Pair,
  OneClickUniV2 as IOneClickUniV2,
  UniswapV2Factory as IUniswapV2Factory,
  StablecompVaultV1 as IStablecompVaultV1,
  UniswapV2Router02 as IUniswapV2Router02,
  StrategyMasterChefLPSpooky as IStrategyMasterChefLPSpooky,
} from '../build/types';
import { setupUser } from '../helpers/ethers';

async function setup() {
  await deployments.fixture([
    'USDC',
    'WrappedFtm',
    'OneClickUniV2',
    'UniswapV2Factory',
    'StablecompVaultV1',
    'UniswapV2Router02',
    'StrategyMasterChefLPSpooky',
  ]);

  const [
    USDC,
    WrappedFtm,
    OneClickUniV2,
    UniswapV2Factory,
    StablecompVaultV1,
    UniswapV2Router02,
    StrategyMasterChefLPSpooky,
  ] = await Promise.all([
    ethers.getContract<IERC20Mock>('USDC'),
    ethers.getContract<IWrappedFtm>('WrappedFtm'),
    ethers.getContract<IOneClickUniV2>('OneClickUniV2'),
    ethers.getContract<IUniswapV2Factory>('UniswapV2Factory'),
    ethers.getContract<IStablecompVaultV1>('StablecompVaultV1'),
    ethers.getContract<IUniswapV2Router02>('UniswapV2Router02'),
    ethers.getContract<IStrategyMasterChefLPSpooky>('StrategyMasterChefLPSpooky'),
  ])

  const accounts = (await getNamedAccounts()) as Accounts;

  return {
    accounts,
    contracts: {
      USDC,
      WrappedFtm,
      OneClickUniV2,
      UniswapV2Factory,
      StablecompVaultV1,
      UniswapV2Router02,
      StrategyMasterChefLPSpooky,
    },
  }
}

describe('Auto composition', () => {
  it('Contract OneClickUniV2 must be deployed', async () => {
    const { contracts } = await setup();
    assert.ok(contracts.OneClickUniV2.address);
  })

  it('Contract StablecompVaultV1 must be deployed', async () => {
    const { contracts } = await setup();
    assert.ok(contracts.StablecompVaultV1.address);
  })

  it('Contract StrategyMasterChefLPSpooky must be deployed', async () => {
    const { contracts } = await setup();
    assert.ok(contracts.StrategyMasterChefLPSpooky.address);
  })

  it('Deposit and withdraw funds with one click function', async () => {
    const { contracts, accounts } = await setup();
    const {
      USDC,
      WrappedFtm,
      OneClickUniV2,
      UniswapV2Factory,
      StablecompVaultV1,
      UniswapV2Router02,
    } = contracts;
    const { user1, deployer } = accounts;

    // Send balances of the owner of the contracts to user1 for a clearer vision throughout the
    // process of entering and exiting the system
    await USDC.transfer(user1, parseEther('100'));

    const balanceUSDC = await USDC.balanceOf(user1);
    // Balance should be greater than 0
    expect(0).to.be.below(balanceUSDC);

    /* Deposit balance in contract OneClickUniV2 to start with the auto-composition */

    // connect user1 to contracts
    const user1Connect = await setupUser(user1, contracts);

    // Token to deposit for auto composition
    const tokenInAmount = parseEther('50');

    // It should be fail because amount deposited es below of minimum
    await expect(
      user1Connect.OneClickUniV2.scompIn(
        StablecompVaultV1.address,
        USDC.address,
        1,
        1,
      )
    ).to.be.revertedWith('Input amount below minimum');

    // It should fail because the OneClickUniV2 contract has not been approved so you can use
    // out tokens
    await expect(
      user1Connect.OneClickUniV2.scompIn(
        StablecompVaultV1.address,
        USDC.address,
        tokenInAmount,
        1,
      )
    ).to.be.revertedWith('Input token is not approved');

    await user1Connect.USDC.approve(OneClickUniV2.address, tokenInAmount);

    await expect(
      user1Connect.OneClickUniV2.scompIn(
        ethers.constants.AddressZero,
        USDC.address,
        tokenInAmount,
        1,
      )
    ).to.be.revertedWith('function call to a non-contract account');

    /* Removed liquidity of the pair */

    const pairAddressUSDC_WFTM = await UniswapV2Factory.getPair(
      USDC.address, WrappedFtm.address
    );

    const pairUSDC_WFTM = await ethers.getContractAt<IUniswapV2Pair>(
      'contracts/thirdPartyContracts/UniswapV2Pair.sol:UniswapV2Pair', pairAddressUSDC_WFTM
    );

    const balanceLiquidity = await pairUSDC_WFTM.balanceOf(deployer);
    await pairUSDC_WFTM.approve(UniswapV2Router02.address, balanceLiquidity);
    await UniswapV2Router02.removeLiquidityETH(
      USDC.address,
      balanceLiquidity,
      1,
      1,
      deployer,
      +new Date()
    );

    // Should fail because have not liquidity in the pool
    await expect(
      user1Connect.OneClickUniV2.scompIn(
        StablecompVaultV1.address,
        USDC.address,
        tokenInAmount,
        1,
      )
    ).to.be.revertedWith('Liquidity pair reserves too low');

    /* Add liquidity to test the entire process */
    const amountETHDesired = parseEther('900');
    const amountTokenDesired = parseEther('1188');
    const deadlineAddLiquidity = +new Date();
    await USDC.approve(UniswapV2Router02.address, amountTokenDesired);
    await UniswapV2Router02.addLiquidityETH(
      USDC.address,
      amountTokenDesired,
      1,
      1,
      deployer,
      deadlineAddLiquidity,
      { value: amountETHDesired }
    )

    // Balance of ownership tokens before using the OneClickUniV2 contract
    const balanceOwnershipTokenBefore = await StablecompVaultV1.balanceOf(user1);
    // Balance of tokens held by the system (LP tokens, and other reward tokens that are part
    // of the strategy)
    const balanceVaultBefore = await StablecompVaultV1.balance();

    // calculate the ideal value for tokenAmountOutMin
    const estimateSwap = await user1Connect.OneClickUniV2.estimateSwap(
      StablecompVaultV1.address,
      USDC.address,
      tokenInAmount
    );
    const tokenAmountOutMin = estimateSwap.swapAmountOut;

    await user1Connect.OneClickUniV2.scompIn(
      StablecompVaultV1.address,
      USDC.address,
      tokenInAmount,
      tokenAmountOutMin
    );

    // Balance of ownership tokens after using the OneClickUniV2 contract
    const balanceOwnershipTokenAfter = await StablecompVaultV1.balanceOf(user1);
    // Balance of tokens held by the system (LP tokens, and other reward tokens that are part
    // of the strategy)
    const balanceVaultAfter = await StablecompVaultV1.balance();

    expect(balanceOwnershipTokenBefore).to.be.below(balanceOwnershipTokenAfter);
    expect(balanceVaultBefore).to.be.below(balanceVaultAfter);

    await user1Connect
      .StablecompVaultV1
      .approve(OneClickUniV2.address, balanceOwnershipTokenAfter);

    await user1Connect.OneClickUniV2.scompOut(
      StablecompVaultV1.address,
      balanceOwnershipTokenAfter
    );
  })
})