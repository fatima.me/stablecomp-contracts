import { parseEther, formatEther } from 'ethers/lib/utils';
import { Accounts } from '../typescript/hardhat';
import { assert, expect } from './utils/chaiSetup';
import { deployments, ethers, getNamedAccounts } from 'hardhat';
import {
  ERC20Mock as IERC20Mock,
  PangolinRouter as IRouter,
  PangolinFactory as IFactory,
  OneClickPangolinV1 as IOneClick,
  StrategyPangolinMiniChefLP as IStrategy,
  StablecompVaultV1 as IStablecompVaultV1,
} from '../build/types';
import { setupUser } from '../helpers/ethers';

async function setup() {
  await deployments.fixture([
    'USDC',
    'USDC.e',
    'PangolinRouter',
    'PangolinFactory',
    'OneClickPangolinV1',
    'VaultPangolin_USDC_USDC.e',
    'StrategyPangolinMiniChefLP_USDC_USDC.e',
  ]);

  const [
    USDC,
    USDCe,
    Router,
    Factory,
    Strategy,
    OneClickV1,
    StablecompVaultV1,
  ] = await Promise.all([
    ethers.getContract<IERC20Mock>('USDC'),
    ethers.getContract<IERC20Mock>('USDC.e'),
    ethers.getContract<IRouter>('PangolinRouter'),
    ethers.getContract<IFactory>('PangolinFactory'),
    ethers.getContract<IStrategy>('StrategyPangolinMiniChefLP_USDC_USDC.e'),
    ethers.getContract<IOneClick>('OneClickPangolinV1'),
    ethers.getContract<IStablecompVaultV1>('VaultPangolin_USDC_USDC.e'),
  ])

  const accounts = (await getNamedAccounts()) as Accounts;

  return {
    accounts,
    contracts: {
      USDC,
      USDCe,
      Router,
      Factory,
      Strategy,
      OneClickV1,
      StablecompVaultV1,
    },
  }
}

describe('Pangolin AMM', () => {
  it('Contract OneClickV1 must be deployed', async () => {
    const { contracts } = await setup();
    assert.ok(contracts.OneClickV1.address);
  })

  it('Contract StablecompVaultV1 must be deployed', async () => {
    const { contracts } = await setup();
    assert.ok(contracts.StablecompVaultV1.address);
  })

  it('Contract Strategy must be deployed', async () => {
    const { contracts } = await setup();
    assert.ok(contracts.Strategy.address);
  })

  it('Deposit and withdraw funds with one click function', async () => {
    const { contracts, accounts } = await setup();
    const {
      USDC,
      USDCe,
      Router,
      OneClickV1,
      StablecompVaultV1,
    } = contracts;
    const { user1, deployer } = accounts;
    const { AddressZero } = ethers.constants;

    // Send balances of the owner of the contracts to user1 for a clearer vision throughout the
    // process of entering and exiting the system
    await USDC.transfer(user1, parseEther('100'));

    const balanceUSDC = await USDC.balanceOf(user1);
    // Balance should be greater than 0
    expect(0).to.be.below(balanceUSDC);

    /* Deposit balance in contract OneClickV1 to start with the auto-composition */

    // connect user1 to contracts
    const user1Connect = await setupUser(user1, contracts);

    // Token to deposit for auto composition
    const tokenInAmount = parseEther('50');

    // It should be fail because amount deposited is below of minimum
    await expect(
      user1Connect.OneClickV1.scompIn(
        StablecompVaultV1.address,
        USDC.address,
        1,
        1,
      )
    ).to.be.revertedWith('Input amount below minimum');

    // It should fail because the OneClickUniV2 contract has not been approved so you can use
    // out tokens
    await expect(
      user1Connect.OneClickV1.scompIn(
        StablecompVaultV1.address,
        USDC.address,
        tokenInAmount,
        1,
      )
    ).to.be.revertedWith('Input token is not approved');

    await user1Connect.USDC.approve(OneClickV1.address, tokenInAmount);

    await expect(
      user1Connect.OneClickV1.scompIn(
        ethers.constants.AddressZero,
        USDC.address,
        tokenInAmount,
        1,
      )
    ).to.be.revertedWith('function call to a non-contract account');

    // Should fail because have not liquidity in the pool
    await expect(
      user1Connect.OneClickV1.scompIn(
        StablecompVaultV1.address,
        USDC.address,
        tokenInAmount,
        1,
      )
    ).to.be.revertedWith('Liquidity pair reserves too low');

    /* Add liquidity to test the entire process */
    const amountTokenADesired = parseEther('900');
    const amountTokenBDesired = parseEther('900');
    const deadlineAddLiquidity = +new Date();
    await USDC.approve(Router.address, amountTokenADesired);
    await USDCe.approve(Router.address, amountTokenBDesired);
    await Router.addLiquidity(
      USDC.address,
      USDCe.address,
      amountTokenADesired,
      amountTokenBDesired,
      1,
      1,
      deployer,
      deadlineAddLiquidity,
    )

    // Balance of ownership tokens before using the OneClickUniV2 contract
    const balanceOwnershipTokenBefore = await StablecompVaultV1.balanceOf(user1);
    // Balance of tokens held by the system (LP tokens, and other reward tokens that are part
    // of the strategy)
    const balanceVaultBefore = await StablecompVaultV1.balance();

    // calculate the ideal value for tokenAmountOutMin
    const estimateSwap = await user1Connect.OneClickV1.estimateSwap(
      StablecompVaultV1.address,
      USDC.address,
      tokenInAmount
    );
    const tokenAmountOutMin = estimateSwap.swapAmountOut;

    await user1Connect.OneClickV1.scompIn(
      StablecompVaultV1.address,
      USDC.address,
      tokenInAmount,
      tokenAmountOutMin
    );

    // Balance of ownership tokens after using the OneClickV2 contract
    const balanceOwnershipTokenAfter = await StablecompVaultV1.balanceOf(user1);
    // Balance of tokens held by the system (LP tokens, and other reward tokens that are part
    // of the strategy)
    const balanceVaultAfter = await StablecompVaultV1.balance();

    expect(balanceOwnershipTokenBefore).to.be.below(balanceOwnershipTokenAfter);
    expect(balanceVaultBefore).to.be.below(balanceVaultAfter);

    await expect(
      user1Connect.OneClickV1.scompOutAndSwap(
        StablecompVaultV1.address,
        AddressZero,
        balanceOwnershipTokenAfter,
        1
      )
    ).to.be.revertedWith('Desired token not present in liquidity pair');

    await user1Connect.StablecompVaultV1.approve(OneClickV1.address, balanceOwnershipTokenAfter);
    await user1Connect.OneClickV1.scompOutAndSwap(
      StablecompVaultV1.address,
      USDC.address,
      balanceOwnershipTokenAfter,
      1
    );
  })
})