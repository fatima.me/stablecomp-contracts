import * as dotenv from 'dotenv';
dotenv.config();

import { HardhatUserConfig } from 'hardhat/types';

// Plugins
import 'hardhat-deploy';
import '@typechain/hardhat';
import 'hardhat-abi-exporter';
import 'hardhat-gas-reporter';
import 'hardhat-contract-sizer';
import '@nomiclabs/hardhat-ethers';
import '@nomiclabs/hardhat-waffle';
import '@tenderly/hardhat-tenderly';
import '@nomiclabs/hardhat-etherscan';
import '@openzeppelin/hardhat-upgrades';

// Tasks
if(!process.env.TYPECHAIN_ON) {
  require('./tasks/vaults');
  require('./tasks/helpers');
}

const {
  INFURA_API_KEY,
  WALLET_PRIVKEY,
  ALCHEMY_API_KEY,
  COINMARKETCAP_API,
  ETHERSCAN_API_KEY,
} = process.env;

function getProviderInfuraURL(network: string) {
  return `https://${network}.infura.io/v3/${INFURA_API_KEY}`
}

// Config
const config: HardhatUserConfig = {
  paths: {
    tests: './test',
    deploy: 'deploy',
    sources: './contracts',
    deployments: 'deployments',
    artifacts: './build/contracts',
  },
  solidity: {
    compilers: [
      {
        version: '0.8.8',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
      {
        version: '0.7.6',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
      {
        version: '0.6.12',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
      {
        version: '0.6.6',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
      {
        version: '0.5.16',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
      {
        version: '0.4.18',
        settings: {
          optimizer: {
            enabled: true,
            runs: 200,
          },
        },
      },
    ],
  },
  defaultNetwork: 'hardhat',
  networks: {
    hardhat: {
      tags: ['local'],
      loggingEnabled: false,
      forking: {
        blockNumber: 14830335,
        url: `https://eth-mainnet.alchemyapi.io/v2/${ALCHEMY_API_KEY}`,
      }
    },
    ropsten: {
      tags: ['staging'],
      loggingEnabled: false,
      accounts: [ WALLET_PRIVKEY ],
      url: getProviderInfuraURL('ropsten'),
    },
    rinkeby: {
      tags: ['staging'],
      accounts: [ WALLET_PRIVKEY ],
      url: getProviderInfuraURL('rinkeby'),
    },
    bsc: {
      chainId: 56,
      accounts: [ WALLET_PRIVKEY ],
      url: "https://bsc-dataseed2.defibit.io/",
    },
    polygon: {
      chainId: 137,
      accounts: [ WALLET_PRIVKEY ],
      url: "https://polygon-rpc.com/",
    },
    mumbai: {
      chainId: 80001,
      accounts: [ WALLET_PRIVKEY ],
      url: 'https://rpc-mumbai.maticvigil.com',
    },
    fuji: {
      chainId: 43113,
      tags: ['staging'],
      accounts: [ WALLET_PRIVKEY ],
      url: 'https://api.avax-test.network/ext/bc/C/rpc',
    },
    bsctestnet: {
      chainId: 97,
      tags: ['staging'],
      accounts: [ WALLET_PRIVKEY ],
      url: "https://data-seed-prebsc-1-s1.binance.org:8545/",
    },
    fantom: {
      chainId: 250,
      url: "https://rpc.ftm.tools",
      accounts: [ WALLET_PRIVKEY ],
    },
    fantomtestnet: {
      chainId: 4002,
      tags: ['staging'],
      url: "https://rpc.testnet.fantom.network",
      accounts: [ WALLET_PRIVKEY ],
    },
    ganache: {
      chainId: 1337,
      url: 'http://localhost:8545',
    },
  },
  etherscan: {
    apiKey: ETHERSCAN_API_KEY,
  },
  gasReporter: {
    currency: 'USD',
    showTimeSpent: true,
    coinmarketcap: COINMARKETCAP_API,
    // outputFile: 'reports/gas-report.log',
    enabled: process.env.REPORT_GAS ? true : false,
  },
  typechain: {
    outDir: 'build/types',
    target: 'ethers-v5',
  },
  abiExporter: {
    path: './build/abis',
    clear: false,
    flat: true,
  },
  tenderly: {
    project: process.env.TENDERLY_PROJECT,
    username: process.env.TENDERLY_USERNAME,
  },
  contractSizer: {
    alphaSort: true,
    runOnCompile: false,
    disambiguatePaths: true,
  },
  namedAccounts: {
    deployer: 0,
    user1: 1,
    user2: 2,
    user3: 3,
  },  
}

export default config
