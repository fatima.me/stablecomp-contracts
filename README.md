# stablecomp-contracts

## What is Stablecomp?
Stablecomp makes it easy for users to assess opportunities to earn compound interest on their stablecoins, make allocations, and collect interest. Users can choose opportunities to stake stablecoins on multiple chains. The platform also provides risk assessment and reward data, allowing users a simple way to allocate their stable coins based on clear risk-reward criteria. All the yields are collected, added up, converted back into Stablecoin and reinvested for the user automatically.

Stablecomp is a project led by a known and accessible group of blockchain finance experts. We at Stablecomp aim to make all stablecoin investment processes easier, starting from researching the best opportunities, on to allocating assets and completing the picture with easy-to-use tracking and analysis tools.

## Enviroment
Install dependencies
```bash
$ yarn install
```

copy the file .env.example you already have the necessary data to run a development environment
```bash
$ cp .env.example .env
```